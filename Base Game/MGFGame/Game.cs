﻿using System;
using MuGameFramework;
using MuGameFramework.Graphics;

namespace MGFGame
{
    class Game : GameCode
    {
        /// <summary>
        /// Initialize objects in the game
        /// </summary>
        public override void Initialize()
        { }

        /// <summary>
        /// Load content for thus objects in the game
        /// Runs before Initialize
        /// </summary>
        public override void LoadContent()
        { }

        /// <summary>
        /// Unload content loaded into the game
        /// </summary>
        public override void UnloadContent()
        { }

        /// <summary>
        /// Update the objects in the game every tick
        /// </summary>
        public override void Update()
        { }

        /// <summary>
        /// Draw the objects in the game every tick
        /// </summary>
        public override void Draw(SpriteBatch spriteBatch)
        {
            //Clear the Spritebatch
            spriteBatch.Clear(Colors.CornflowerBlue);
        }
    }
}
