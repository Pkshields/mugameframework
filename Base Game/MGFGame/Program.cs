﻿using System;
using System.Windows.Forms;
using MuGameFramework;

namespace MGFGame
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Default C# entry methods
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Get and instance of this game's GameCode
            Game game = new Game();

            //Start the engine
            using (GameFramework engine = new GameFramework(game))
            {
                engine.StartEngine();
            }
        }
    }
}
