# MuGameFramework
 
MuGameFramework is a 2D game framework, built upon C# and SFML, that integrates many features that are used in video games such as window management, Sprite management, sound, Input devices, content caching and more. It is built to support both .NET and Mono.

MuGameFramework  is licensed under the New BSD License.