﻿///////////////////////////////////////////////////////////////////////////////////
/// Quick note: this is made to show some of the capabilities of MuGameFramework, 
/// not necessarly for game design tips. Take nay and all actual code here with a 
/// pinch of salt!
///////////////////////////////////////////////////////////////////////////////////

using System;
using MuGameFramework;
using MuGameFramework.Graphics;
using MuGameFramework.Graphics.Shapes;
using MuGameFramework.InputDevice;
using MuGameFramework.Components;
using MuGameFramework.Audio;

namespace MGFGame
{
    class Game : GameCode
    {
        private Sprite car;
        private RectangleShape rectShape;
        private Music song;
        private KeyboardState keyboardState;
        private int speed = 5;

        private bool mPressed = false;

        /// <summary>
        /// Set the general MuGameFramework settings
        /// </summary>
        /// <returns>The set settings</returns>
        public override GameSettings GameSettings()
        {
            //Just using the default settings
            return new GameSettings()
            {

            };
        }

        /// <summary>
        /// Initialize objects in the game
        /// </summary>
        public override void Initialize()
        {
            //Create a drawable rectangle
            rectShape = new RectangleShape(100, 100);
            rectShape.Position = new Vector2(700, 400);
            rectShape.FillColor = Colors.BlanchedAlmond;

            //Add it to services, just to show the functionality
            Services.AddService(rectShape);

            //Pull the object back out of Services
            //Usually this would be done in another class where the RectangleShape wouldn't exist
            RectangleShape pulledRect = Services.GetService<RectangleShape>();
        }

        /// <summary>
        /// Load content for thus objects in the game
        /// Runs before Initialize
        /// </summary>
        public override void LoadContent()
        {
            //Create a drawable sprite using a Texture2D
            Texture2D carTexture = Content.Load<Texture2D>(@"car1.gif");
            car = new Sprite(carTexture);
            car.Origin = new Vector2(car.FullSize.Width / 2, car.FullSize.Height / 2);

            //Create a Music object to stream a song off the hard drive (rather than loading into memory)
            song = Content.Load<Music>(@"bgsong.ogg");
            song.Play();
        }

        /// <summary>
        /// Unload content loaded into the game
        /// </summary>
        public override void UnloadContent()
        {
            //Unload the objects in the heap
            car.Dispose();
            rectShape.Dispose();
            song.Dispose();
        }

        /// <summary>
        /// Update the objects in the game every tick
        /// </summary>
        public override void Update()
        {
            //Get the lates keyboard state
            keyboardState = Keyboard.GetState();

            ///Check the four arrow keya, checkign if the car should move and in what direction
            if (keyboardState.IsKeyDown(Key.Up))
            {
                car.Position += new Vector2(0, -1 * speed);
                car.Rotation = 0;
            }

            if (keyboardState.IsKeyDown(Key.Down))
            {
                car.Position += new Vector2(0, speed);
                car.Rotation = 180;
            }

            if (keyboardState.IsKeyDown(Key.Left))
            {
                car.Position += new Vector2(-1 * speed, 0);
                car.Rotation = 270;
            }

            if (keyboardState.IsKeyDown(Key.Right))
            {
                car.Position += new Vector2(speed, 0);
                car.Rotation = 90;
            }

            //Alternate through playing and pausing the music track
            //Simple bool used to detect that the button is only detected on press and not constantly
            if (keyboardState.IsKeyDown(Key.M))
            {
                if (!mPressed)
                {
                    switch (song.Status)
                    {
                        case SoundStatus.Paused: song.Play();
                            break;
                        case SoundStatus.Playing: song.Pause();
                            break;
                    }

                    mPressed = true;
                }
            }
            else
                mPressed = false;

            //Also, constantly rotate the rectangle
            rectShape.Rotation = (rectShape.Rotation + 3) % 360;
        }

        /// <summary>
        /// Draw the objects in the game every tick
        /// </summary>
        public override void Draw(SpriteBatch spriteBatch)
        {
            //Clear the Spritebatch
            spriteBatch.Clear(Colors.CornflowerBlue);

            //Draw a string, the shape and sprite on screen
            spriteBatch.DrawString(Font.DefaultFont, "Press M to Play/Pause the music", new Vector2(300, 50), Colors.Firebrick);
            spriteBatch.DrawShape(rectShape);
            spriteBatch.Draw(car);
        }
    }
}
