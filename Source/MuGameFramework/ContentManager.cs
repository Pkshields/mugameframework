﻿using System;
using System.Collections.Generic;
using System.IO;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Manager used to load content into an object and into memory
    /// </summary>
    public class ContentManager : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// Directory that all content should be contained in
        /// </summary>
        private string graphicsDir = @"";

        /// <summary>
        /// Dictionary that caches all currently loaded content
        /// </summary>
        private Dictionary<string, IContent> contentDict = new Dictionary<string, IContent>();

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the ContentManager
        /// </summary>
        /// <param name="contentDirectory">Directory that all content is contained in</param>
        public ContentManager(string contentDirectory)
        {
            graphicsDir = AppDomain.CurrentDomain.BaseDirectory + contentDirectory;
        }

                                                #endregion CONSTRUCTORS

                                                #region LOAD METHODS

        /// <summary>
        /// Load a piece of content from the content directory
        /// </summary>
        /// <typeparam name="T">Type of content to load</typeparam>
        /// <param name="fileToLoad">Name of file to load</param>
        /// <returns>Loaded file</returns>
        public T Load<T>(string fileToLoad) where T : IContent
        {
            //First check if the content is in the cache
            if (contentDict.ContainsKey(fileToLoad))
            {
                //It is! Get it out of the cache
                IContent content = contentDict[fileToLoad];

                //Check that is isn't already disposed
                //If it is, then remove it from the dictionary and continue with reloading it
                //Else, return the cached object
                if (content.IsDisposed())
                    contentDict.Remove(fileToLoad);
                else
                    return (T)(object)content;
            }

            //2nd, if it isn't already in the cache, check that the file exists.
            //If it doesn't then throw an exception
            if (!File.Exists(graphicsDir + @"/" + fileToLoad))
                throw new ArgumentException("File does not exist");

            //3rd, load the content from the file
            try
            {
                //Create an object of the required type, load the content into it, add it to the cache and return it to the user
                T content = (T)Activator.CreateInstance(typeof(T), true);
                content.LoadContent(graphicsDir + @"/" + fileToLoad);
                contentDict.Add(fileToLoad, content);
                return (T)(object)content;
            }
            catch (Exception ex)
            {
                //Type is not supported, throw an exception
                throw new ArgumentException("Type is not supported by the Content Manager", ex);
            }
        }

        /// <summary>
        /// Unload the content from the game
        /// </summary>
        /// <param name="content">Object to unload</param>
        public void Unload(IContent content)
        {
            //Dispose the file and remove it from the cache via a loop
            content.Dispose();
            foreach (var findContent in contentDict)
                if (findContent.Value == content)
                    contentDict.Remove(findContent.Key);
        }

        /// <summary>
        /// Unload the content from the game
        /// </summary>
        /// <param name="fileToUnload">File To Unload</param>
        public void Unload(string fileToUnload)
        {
            //Dispose the file and remove it from the cache
            contentDict[fileToUnload].Dispose();
            contentDict.Remove(fileToUnload);
        }
        

                                                #endregion LOAD METHODS

                                                #region DISPOSE

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Managed components
            if (disposing)
            {
                if (contentDict != null)
                {
                    //Unload all the content objects
                    foreach (var content in contentDict)
                        content.Value.Dispose();

                    //Then clear the dictionary
                    contentDict.Clear();
                }
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~ContentManager()
        {
            Dispose(false);
        }

                                                #endregion DISPOSE
    }
}
