﻿using System;
using MuGameFramework.Components;
using MuGameFramework.Graphics;

namespace MuGameFramework.Diagnostics
{
    /// <summary>
    /// Provides a visual FPS counter on screen
    /// </summary>
    public class FPSCounter : GameComponent
    {
                                                #region PROPERTIES

        /// <summary>
        /// Current framerate of the game
        /// </summary>
        public uint FrameRate
        {
            get;
            private set;
        }

        /// <summary>
        /// Elapsed time since last ramerate count
        /// </summary>
        private int elapsedTime;

        /// <summary>
        /// Number of times Draw() has been called since last framerate update
        /// </summary>
        private uint updateCalls;

        /// <summary>
        /// Position to draw the framerate string at
        /// </summary>
        private Vector2 position;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of the FPSCounter object
        /// </summary>
        /// <param name="position">Position to draw the framerate string at</param>
        /// <param name="game">Game that is running this</param>
        public FPSCounter(Vector2 position, GameCode game)
            : base(game)
        {
            this.position = position;
            FrameRate = 0;
            elapsedTime = 0;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Update framerate and draw the FPS counter on screen
        /// </summary>
        /// <param name="gameTime">Current gameTime object</param>
        /// <param name="spriteBatch">Spritebatch used to draw stuff on screen</param>
        /// <param name="preText">Text to show before the actual framerate value</param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch, string preText = "")
        {
            //Update the elapsed time
            elapsedTime += gameTime.ElapsedTime.Milliseconds;

            //If the elapsed time is mreo than a second, calculate the framerate + reset
            if (elapsedTime >= 1000)
            {
                FrameRate = updateCalls;
                updateCalls = 0;
                elapsedTime = 0;
            }

            //Increment calls made to this update
            updateCalls++;

            //Draw the counter on screen
            spriteBatch.DrawString(Font.DefaultFont, preText + FrameRate, position, Colors.Red);
        }

        /// <summary>
        /// Update framerate
        /// </summary>
        /// <param name="gameTime">Current gameTime object</param>
        public void Draw(GameTime gameTime)
        {
            //Update the elapsed time
            elapsedTime += gameTime.ElapsedTime.Milliseconds;

            //If the elapsed time is mreo than a second, calculate the framerate + reset
            if (elapsedTime >= 1000)
            {
                FrameRate = updateCalls;
                updateCalls = 0;
                elapsedTime = 0;
            }

            //Increment calls made to this update
            updateCalls++;
        }

                                                #endregion METHODS
    }
}
