﻿using System;
using MuGameFramework;
using MuGameFramework.Graphics;
using SFMLImage = SFML.Graphics.Image;
using SFMLTexture = SFML.Graphics.Texture;

namespace MuGameFramework.Diagnostics
{
    /// <summary>
    /// Static class allowing you to take a screenshot of the screen
    /// </summary>
    public static class Screenshot
    {
        /// <summary>
        /// Take a screenshot and save it to a file
        /// </summary>
        /// <param name="window">Window to take a screenshot from</param>
        /// <param name="locationFileName">Location relative to the root directory here + name of the file</param>
        public static void TakeScreenshot(Window window, string locationFileName)
        {
            //Take the screenshot, store it in a SFMLTexture, save it to a file then dispose of it from memory
            SFMLImage screenshot;
            screenshot = window.RenderWindow.Capture();
            screenshot.SaveToFile(AppDomain.CurrentDomain.BaseDirectory + locationFileName);
            screenshot.Dispose();
        }

        /// <summary>
        /// Take a screenshot and store it in a Texture2D
        /// </summary>
        /// <param name="window">Window to take a screenshot from</param>
        /// <returns>Texture2D containing the screenshot</returns>
        public static Texture2D TakeScreenshot(Window window)
        {
            SFMLImage screenshot;
            screenshot = window.RenderWindow.Capture();
            return new Texture2D(new SFMLTexture(screenshot));
        }
    }
}
