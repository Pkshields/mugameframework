﻿using System;
using System.Collections.Generic;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Object that can update and draw GameComponents automatically
    /// </summary>
    public class GameComponentList : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// List used to store the components
        /// </summary>
        private List<GameComponent> componentList;

        /// <summary>
        /// Get the number of components stored
        /// </summary>
        public int Count
        {
            get { return componentList.Count; }
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the GameComponentList
        /// </summary>
        internal GameComponentList()
        {
            //Initialize the list used to store the game components
            componentList = new List<GameComponent>();
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Add a GameComponent to the list
        /// </summary>
        /// <param name="component">GameComponent to add</param>
        public void Add(GameComponent component)
        {
            //A few useful variables
            bool addCheck = false;
            int componentCount = componentList.Count;

            //Loop through all current gamecomponents
            for(int i = 0; i < componentCount; i++)
            {
                //Check the gamecomponent ordering
                //If the component order is more than the component we are going to add, thn add it before this item to keep the order correct.
                if (componentList[i].updateOrder > component.updateOrder)
                {
                    //Add the component and break the loop
                    componentList.Insert(i, component);
                    addCheck = true;
                    break;
                }
            }

            //If the component was not added in the loop anywhere, add it to the end of the list
            if (!addCheck)
                componentList.Add(component);

            //Run the initialise and loadcontentent methods
            component.LoadContent();
            component.Initialize();
        }

        /// <summary>
        /// Clear the components list
        /// </summary>
        public void Clear()
        {
            componentList.Clear();
        }

        /// <summary>
        /// Update the components
        /// </summary>
        internal void Update()
        {
            //Run the update method on each of the components
            foreach (GameComponent component in componentList)
                component.Update();
        }

        /// <summary>
        /// Draw the components
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw the objects</param>
        internal void Draw(SpriteBatch spriteBatch)
        {
            //Run the Draw method on each of the components
            foreach (GameComponent component in componentList)
                component.Draw(spriteBatch);
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Managed components
            if (disposing)
            {
                if (componentList != null)
                {
                    //Unload all the GameComponents stored in the GameComponentList
                    foreach (GameComponent component in componentList)
                        component.Dispose();

                    //Then dispose of the GameComponentList
                    componentList.Clear();
                    componentList = null;
                }
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~GameComponentList()
        {
            Dispose(false);
        }

                                                #endregion INTERFACE METHODS
    }
}
