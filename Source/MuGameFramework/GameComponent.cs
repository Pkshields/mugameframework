﻿using System;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Representing a single drawable component in the game
    /// </summary>
    public abstract class GameComponent : IUpdatable, IDrawable, IDisposable
    {

                                                #region PROPERTIES

        /// <summary>
        /// Access to the GameCode and everything it contains
        /// </summary>
        protected GameCode game;

        /// <summary>
        /// Order in the GameComponent list to update and draw this object at
        /// </summary>
        internal int updateOrder;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize this instance of GameComponent
        /// </summary>
        /// <param name="game">The GameCode that contains this object</param>
        /// <param name="updateOrder">Order in the GameComponent list to update and draw this object at</param>
        public GameComponent(GameCode game, int updateOrder = 0)
        {
            this.game = game;
            this.updateOrder = updateOrder;
        }

                                                #endregion CONSTRUCTORS

                                                #region VIRTUAL METHODS

        /// <summary>
        /// Initialize objects in the game
        /// </summary>
        public virtual void Initialize()
        { }

        /// <summary>
        /// Load content for thus objects in the game
        /// Runs before Initialize
        /// </summary>
        public virtual void LoadContent()
        { }

        /// <summary>
        /// Unload content loaded into the game
        /// </summary>
        public virtual void UnloadContent()
        { }

        /// <summary>
        /// Update the objects in the game every tick
        /// </summary>
        public virtual void Update()
        { }

        /// <summary>
        /// Draw the objects in the game every tick
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw the objects</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        { }

                                                #endregion VIRTUAL METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Different that the otehr Dispose methods. Made more for overriding. By default just calls the UnloadContent method
            UnloadContent();
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~GameComponent()
        {
            Dispose(false);
        }

                                                #endregion INTERFACE METHODS
    }
}
