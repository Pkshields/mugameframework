using System;
using System.Diagnostics;

namespace MuGameFramework
{
    /// <summary>
    /// Container class for the Stopwatch?
    /// </summary>
    class Timer
    {
                                                #region PROPERTIES

        /// <summary>
        /// Stopwatch used to control the time
        /// </summary>
        Stopwatch stopwatch;

        /// <summary>
        /// Get the number of milliseconds elapsed since start
        /// </summary>
        public long ElapsedMilliseconds
        {
            get { return stopwatch.ElapsedMilliseconds; }
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTOR

        /// <summary>
        /// Start a new instance of Timer
        /// </summary>
        public Timer()
        {
            //Create the stopwatch and instantly reset it for good measure
            stopwatch = new Stopwatch();
            stopwatch.Reset();
        }

                                                #endregion CONSTRUCTOR

                                                #region METHODS

        /// <summary>
        /// Start the timer
        /// </summary>
        public void Start()
        {
            //Make sure the stopwatch isn't already running
            if (!stopwatch.IsRunning)
            {
                //Rsset the stopwatch to make sure then start it
                stopwatch.Reset();
                stopwatch.Start();
            }
        }

        /// <summary>
        /// Reset the timer to 0 and restart it
        /// </summary>
        public void Reset()
        {
            stopwatch.Restart();
        }

        /// <summary>
        /// Stop the timer
        /// </summary>
        public void Stop()
        {
            stopwatch.Stop();
        }

                                                #endregion METHODS
    }
}
