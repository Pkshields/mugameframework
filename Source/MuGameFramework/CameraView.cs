﻿using System;
using MuGameFramework.Components;
using SFMLView = SFML.Graphics.View;
using SFMLVector2f = SFML.Window.Vector2f;

namespace MuGameFramework
{
    /// <summary>
    /// Object representing a region of 2D space to show on screen
    /// </summary>
    public class CameraView : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML View
        /// </summary>
        internal SFMLView SFMLView;

        /// <summary>
        /// Position of the center of the View
        /// </summary>
        public Vector2 Center
        {
            //In each of these, the axis is converted to a default programming style axis
            get 
            {
                SFMLVector2f center = SFMLView.Center * -1;
                center += SFMLView.Size;
                return new Vector2(center.X, center.Y);
            }
            set
            {
                SFMLVector2f center = value.AsSFMLVector2f - SFMLView.Size;
                center *= -1;
                SFMLView.Center = center;
            }
        }

        /// <summary>
        /// Size of the view
        /// </summary>
        public Vector2 Size
        {
            get { return new Vector2(SFMLView.Size.X, SFMLView.Size.Y); }
            set { SFMLView.Size = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Rotation of the view in degrees
        /// </summary>
        public float Rotation
        {
            get { return SFMLView.Rotation; }
            set { SFMLView.Rotation = value; }
        }

        /// <summary>
        /// Local record of the current Zoom level
        /// </summary>
        private float zoom;


                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new CameraView of as apecific size
        /// </summary>
        /// <param name="size">Size of the CameraView</param>
        public CameraView(Vector2 size)
        {
            Vector2 halfSize = size / 2;
            SFMLView = new SFMLView(halfSize.AsSFMLVector2f, size.AsSFMLVector2f);
        }

        /// <summary>
        /// Encapsulate an SFMLView suign a CameraView
        /// </summary>
        /// <param name="encapsulateView">SFML View to encapsulate</param>
        internal CameraView(SFMLView encapsulateView)
        {
            SFMLView = encapsulateView;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Move the CameraView by a relative vector
        /// </summary>
        /// <param name="relativeOffset">Amount to move</param>
        public void Move(Vector2 relativeOffset)
        {
            //Convert from a real axis to the view one then apply
            relativeOffset *= -1;
            SFMLView.Move(relativeOffset.AsSFMLVector2f);
        }

        /// <summary>
        /// Get the current Zoom level
        /// </summary>
        /// <returns>The current zoom level</returns>
        public float GetZoom()
        {
            return zoom;
        }

        /// <summary>
        /// Set the zoom level
        /// </summary>
        /// <param name="zoom">New level of zoom</param>
        public void SetZoom(float zoom)
        {
            SFMLView.Zoom(zoom);
            this.zoom = zoom;
        }

        /// <summary>
        /// Convert screen coordinates (such as mouse position) to View positions
        /// </summary>
        /// <param name="position">Screen position</param>
        /// <returns>View position</returns>
        public Vector2 ConvertCoords(Vector2 position)
        {
            Vector2 topLeft = Center - (Size / 2);
            return position - topLeft;
        }

                                                #endregion METHODS

                                                #region DISPOSE

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Managed components
            if (disposing)
            {
                if (SFMLView != null)
                {
                    SFMLView.Dispose();
                    SFMLView = null;
                }
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~CameraView()
        {
            Dispose(false);
        }

                                                #endregion DISPOSE
    }
}
