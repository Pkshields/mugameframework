using System;
using MuGameFramework.Components;
using SFML.Graphics;
using SFML.Window;

namespace MuGameFramework
{
    /// <summary>
    /// Container class allowing creation and access of the Window
    /// </summary>
    public class Window : IRenderTarget
    {
                                                #region PROPERTIES

        /// <summary>
        /// Provides a RenderWindow to draw objects on
        /// </summary>
        private RenderWindow window;
        internal RenderWindow RenderWindow
        {
            get { return window; }
        }

        /// <summary>
        /// Get the default view of this window
        /// </summary>
        public CameraView DefaultView
        {
            get { return new CameraView(window.DefaultView); }
        }

        /// <summary>
        /// Special member that is used for most of the non-visual controls, such as Notify or Timer.
        /// Added to this just in case currently
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Get the dimensions of the Window
        /// </summary>
        public Size WindowDimensions
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the position of the window
        /// </summary>
        public Point WindowPosition
        {
            get;
            private set;
        }

        /// <summary>
        /// Defines if the window is currently open or not
        /// </summary>
        public bool IsOpen
        {
            get;
            private set;
        }

        /// <summary>
        /// Defines if the window has focus or not
        /// </summary>
        public bool HasFocus
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Generate a new window for the game to run in windowed mode
        /// </summary>
        /// <param name="windowName">Title for the window</param>
        /// <param name="sizeloc">Size of the window</param>
        public Window(string windowName, Rectangle sizeloc)
        {
            Initialize(windowName, sizeloc, false);
        }

        /// <summary>
        /// Generate a new window for the game to run in fullscreen mode
        /// </summary>
        /// <param name="windowName">Title for the window</param>
        public Window(string windowName)
        {
            Initialize(windowName, null, true);
        }

        /// <summary>
        /// Initial method for generating a window
        /// </summary>
        /// <param name="windowName">Title for the window</param>
        /// <param name="sizePos">Size and position of the window</param>
        /// <param name="isFullscreen">Should the window be fullscreen</param>
        private void Initialize(string windowName, Rectangle? sizePos, bool isFullscreen)
        {
            //Initialize the Render Window
            //If it is fullscreen, initialize accordingly
            if (isFullscreen)
            {
                //Initialize the window with the best screen mode and store the window dimensions for use by the user
                VideoMode mode = VideoMode.FullscreenModes[0];
                window = new RenderWindow(mode, windowName, Styles.Fullscreen);
                WindowDimensions = new Size((int)mode.Width, (int)mode.Height);
            }

            //Else, initialize with the set dimensions
            else
            {
                //Check that the passed in dimensions are not null
                if (sizePos.HasValue)
                {
                    //Initialize the window with the set screen resolution and store the window dimensions for use by the user
                    window = new RenderWindow(new VideoMode((uint)sizePos.Value.Width, (uint)sizePos.Value.Height, 32), windowName, Styles.Close);
                    WindowDimensions = new Size(sizePos.Value.Width, sizePos.Value.Height);

                    //Set the position of the window
                    window.Position = new Vector2i(sizePos.Value.X, sizePos.Value.Y);
                    WindowPosition = new Point(sizePos.Value.X, sizePos.Value.Y);
                }

                else
                    //Throw an exception if they are
                    throw new ArgumentNullException("Window Size is not set and window is not fullscreen.");
            }

            //Enable VSync
            window.SetVerticalSyncEnabled(true);

            //Set the window as open and it has focus
            IsOpen = true;
            HasFocus = true;

            //Window Events
            window.Closed += new EventHandler(window_Closed);
            window.LostFocus += new EventHandler(window_LostFocus);
            window.GainedFocus += new EventHandler(window_GainedFocus);
            window.Resized += new EventHandler<SizeEventArgs>(window_Resized);
        }

                                                #endregion CONSTRUCTORS

                                                #region EVENT METHODS

        /// <summary>
        /// Close the window and notify the engine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_Closed(object sender, EventArgs e)
        {
            CloseWindow();
        }

        /// <summary>
        /// Notify the engine that we have lost focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_LostFocus(object sender, EventArgs e)
        {
            HasFocus = false;
        }

        /// <summary>
        /// Notify the engine that we have gained focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_GainedFocus(object sender, EventArgs e)
        {
            HasFocus = true;
        }

        /// <summary>
        /// Update the WindowDimensions object on resize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_Resized(object sender, SizeEventArgs e)
        {
            WindowDimensions = new Size((int)e.Width, (int)e.Height);
        }

                                                #endregion EVENT METHODS

                                                #region METHODS

        /// <summary>
        /// Call the DispatchEvents method to update all the events for this window
        /// </summary>
        public void DispatchEvents()
        {
            window.DispatchEvents();
        }

        /// <summary>
        /// Call the Display function to update what is drawn on screen
        /// </summary>
        public void Display()
        {
            window.Display();
        }

        /// <summary>
        /// Close the window and notify the engine
        /// </summary>
        public void CloseWindow()
        {
            window.Close();
            IsOpen = false;
        }

        /// <summary>
        /// Change the current CameraView of this window
        /// </summary>
        /// <param name="view">New CameraView to set</param>
        public void ChangeCameraView(CameraView view)
        {
            window.SetView(view.SFMLView);
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS


        /// <summary>
        /// Get the RenderTarget for the SpriteBatch to use
        /// </summary>
        /// <returns>RenderTarget for the SpriteBatch to use</returns>
        RenderTarget IRenderTarget.GetRenderTarget()
        {
            return window;
        }

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Managed components
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                    components = null;
                }
            }

            //Dispose of Unmanaged or large components
            if (window != null)
            {
                window.Dispose();
                window = null;
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Window()
        {
            Dispose(false);
        }

                                                #endregion INTERFACE METHODS
    }
}
