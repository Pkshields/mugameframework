﻿using System;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Interface ensuring all drawable classes have the Draw methods
    /// </summary>
    public interface IDrawable
    {
        /// <summary>
        /// Load content for thus objects in the game
        /// Runs before Initialize
        /// </summary>
        void LoadContent();

        /// <summary>
        /// Unload content loaded into the game
        /// </summary>
        void UnloadContent();

        /// <summary>
        /// Draw the objects in the game every tick
        /// </summary>
        void Draw(SpriteBatch spriteBatch);
    }
}
