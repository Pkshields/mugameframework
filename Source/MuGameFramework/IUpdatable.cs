﻿using System;

namespace MuGameFramework
{
    /// <summary>
    /// Interface ensuring all updatable classes have the Update methods
    /// </summary>
    public interface IUpdatable
    {
        /// <summary>
        /// Initialize objects in the game
        /// </summary>
        void Initialize();

        /// <summary>
        /// Update the objects in the game every tick
        /// </summary>
        void Update();
    }
}
