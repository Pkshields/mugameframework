﻿using System;
using SFMLRenderTarget = SFML.Graphics.RenderTarget;

namespace MuGameFramework
{
    /// <summary>
    /// Interface ensuring all RenderTargets can return the SFML RenderTarget the Framework
    /// </summary>
    public interface IRenderTarget : IDisposable
    {
        /// <summary>
        /// Get the RenderTarget for the SpriteBatch to use
        /// </summary>
        /// <returns>RenderTarget for the SpriteBatch to use</returns>
        SFMLRenderTarget GetRenderTarget();
    }
}
