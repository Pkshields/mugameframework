﻿using System;
using SFMLSoundStatus = SFML.Audio.SoundStatus;

namespace MuGameFramework.Audio
{
    /// <summary>
    /// Status for a current sound object
    /// </summary>
    public enum SoundStatus
    {
        Paused = SFMLSoundStatus.Paused,
        Playing = SFMLSoundStatus.Playing, 
        Stopped = SFMLSoundStatus.Stopped
    }
}
