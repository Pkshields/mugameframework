﻿using System;
using SFMLMusic = SFML.Audio.Music;
using SFMLSoundBuffer = SFML.Audio.SoundBuffer;

namespace MuGameFramework.Audio
{
    /// <summary>
    /// Music object used for large music files, streaming from the file system
    /// </summary>
    public class Music : IContent
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Music object
        /// </summary>
        internal SFMLMusic SFMLMusic;

        /// <summary>
        /// Total duration of the music file
        /// </summary>
        public TimeSpan Duration
        {
            get { return SFMLMusic.Duration; }
        }

        /// <summary>
        /// Get the number of channels used by the music
        /// </summary>
        public uint ChannelCount
        {
            get { return SFMLMusic.ChannelCount; }
        }

        /// <summary>
        /// Get the sample rate of the music file
        /// </summary>
        public uint SampleRate
        {
            get { return SFMLMusic.SampleRate; }
        }

        /// <summary>
        /// Get the current status of the Music object
        /// </summary>
        public SoundStatus Status
        {
            get { return (SoundStatus)SFMLMusic.Status; }
        }

        /// <summary>
        /// Get the current time position in the music object
        /// </summary>
        public TimeSpan PlayPosition
        {
            get { return SFMLMusic.PlayingOffset; }
            set { SFMLMusic.PlayingOffset = value; }
        }

        /// <summary>
        /// Set if the music should loop when finished
        /// </summary>
        public bool Loop
        {
            get { return SFMLMusic.Loop; }
            set { SFMLMusic.Loop = value; }
        }

        /// <summary>
        /// Set the pitch of the music object
        /// </summary>
        public float Pitch
        {
            get { return SFMLMusic.Pitch; }
            set { SFMLMusic.Pitch = value; }
        }

        /// <summary>
        /// Set the volume of the music object
        /// </summary>
        public float Volume
        {
            get { return SFMLMusic.Volume; }
            set { SFMLMusic.Volume = value; }
        }

        /// <summary>
        /// Find out the attenuation of the music object
        /// </summary>
        public float Attenuation
        {
            get { return SFMLMusic.Attenuation; }
            set { SFMLMusic.Attenuation = value; }
        }

        /// <summary>
        /// Is this Music disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new Music (Used only in ContentManager)
        /// This plays music without loading it all in memory, for large music files
        /// </summary>
        internal Music()
        {
            //Content is not loaded yet, let on that the object is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Create a new Music
        /// This plays music without loading it all in memory, for large music files
        /// </summary>
        /// <param name="fileToLoad">Music file to store</param>
        internal Music(string fileToLoad)
        {
            this.SFMLMusic = new SFMLMusic(fileToLoad);
            this.Disposed = false;
        }

        /// <summary>
        /// Create a new Music
        /// This plays music without loading it all in memory, for large music files
        /// </summary>
        /// <param name="music">SFML Music to store</param>
        internal Music(SFMLMusic music)
        {
            //Store the texture, then the size in a MGF friendly format
            this.SFMLMusic = music;
            this.Disposed = false;
        }

        /// <summary>
        /// Load a file into this object (if file is not already loaded into it)
        /// </summary>
        /// <param name="fileToLoad">Location of file to load</param>
        void IContent.LoadContent(string fileToLoad)
        {
            this.SFMLMusic = new SFMLMusic(fileToLoad);
            this.Disposed = false;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Play the Music
        /// </summary>
        public void Play()
        {
            SFMLMusic.Play();
        }

        /// <summary>
        /// Pause the Music
        /// </summary>
        public void Pause()
        {
            SFMLMusic.Pause();
        }

        /// <summary>
        /// Stop the Music
        /// </summary>
        public void Stop()
        {
            SFMLMusic.Stop();
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (SFMLMusic != null)
            {
                SFMLMusic.Dispose();
                SFMLMusic = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Music()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Music disposed?
        /// </summary>
        /// <returns>Is this Music disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
