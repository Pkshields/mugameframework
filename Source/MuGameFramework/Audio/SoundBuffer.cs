﻿using System;
using SFMLSoundBuffer = SFML.Audio.SoundBuffer;

namespace MuGameFramework.Audio
{
    /// <summary>
    /// SoundBuffer object for holding a music file in memory
    /// </summary>
    public class SoundBuffer : IContent
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML SoundBuffer used to store the audio file
        /// </summary>
        internal SFMLSoundBuffer soundBuffer
        {
            get;
            private set;
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the number of channels used by the sound
        /// </summary>
        public uint ChannelCount
        {
            get { return soundBuffer.ChannelCount; }
        }

        /// <summary>
        /// Get the duration of the sound file
        /// </summary>
        public uint Duration
        {
            get { return soundBuffer.Duration; }
        }

        /// <summary>
        /// Get the sample rate of the sound file
        /// </summary>
        public uint SampleRate
        {
            get { return soundBuffer.SampleRate; }
        }

        /// <summary>
        /// Get the number of samples used by the sound file
        /// </summary>
        public int SampleCount
        {
            get { return soundBuffer.Samples.Length; }
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTOR

        /// <summary>
        /// Create a new AudioObject (Used only in ContentManager)
        /// </summary>
        internal SoundBuffer()
        {
            //Content is not loaded yet, let on that the object is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Create a new AudioObject
        /// </summary>
        /// <param name="fileToLoad">SFML SoundBuffer to store</param>
        internal SoundBuffer(string fileToLoad)
        {
            this.soundBuffer = new SFMLSoundBuffer(fileToLoad);
            this.Disposed = false;
        }

        /// <summary>
        /// Create a new AudioObject
        /// </summary>
        /// <param name="soundBuffer">SFML sound to store via buffer</param>
        internal SoundBuffer(SFMLSoundBuffer soundBuffer)
        {
            //Store the texture, then the size in a MGF friendly format
            this.soundBuffer = soundBuffer;
            this.Disposed = false;
        }

        /// <summary>
        /// Load a file into this object (if file is not already loaded into it)
        /// </summary>
        /// <param name="fileToLoad">Location of file to load</param>
        void IContent.LoadContent(string fileToLoad)
        {
            this.soundBuffer = new SFMLSoundBuffer(fileToLoad);
            this.Disposed = false;
        }

                                                #endregion CONSTRUCTOR

                                                #region METHODS

        /// <summary>
        /// Get a new copy of this sound effect to play
        /// </summary>
        /// <returns>New copy of this sound effect</returns>
        public Sound GetSoundEffect()
        {
            return new Sound(this);
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS


        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (soundBuffer != null)
            {
                soundBuffer.Dispose();
                soundBuffer = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~SoundBuffer()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this AudioObject disposed?
        /// </summary>
        /// <returns>Is this AudioObject disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
