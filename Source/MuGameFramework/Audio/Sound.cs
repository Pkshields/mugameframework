﻿using System;
using SFMLSound = SFML.Audio.Sound;
using SFMLSoundBuffer = SFML.Audio.SoundBuffer;

namespace MuGameFramework.Audio
{
    /// <summary>
    /// Sound object for playing a Soundbuffer from mebory - used for small soundbytes, sound effects
    /// </summary>
    public class Sound : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Sound object
        /// </summary>
        internal SFMLSound SFMLSound;

        /// <summary>
        /// Get the current status of the Audio object
        /// </summary>
        public SoundStatus Status
        {
            get { return (SoundStatus)SFMLSound.Status; }
        }

        /// <summary>
        /// Get the current time position in the audio object
        /// </summary>
        public TimeSpan PlayPosition
        {
            get { return SFMLSound.PlayingOffset; }
            set { SFMLSound.PlayingOffset = value; }
        }

        /// <summary>
        /// Set if the audio should loop when finished
        /// </summary>
        public bool Loop
        {
            get { return SFMLSound.Loop; }
            set { SFMLSound.Loop = value; }
        }

        /// <summary>
        /// Set the pitch of the audio object
        /// </summary>
        public float Pitch
        {
            get { return SFMLSound.Pitch; }
            set { SFMLSound.Pitch = value; }
        }

        /// <summary>
        /// Set the volume of the audio object
        /// </summary>
        public float Volume
        {
            get { return SFMLSound.Volume; }
            set { SFMLSound.Volume = value; }
        }

        /// <summary>
        /// Find out the attenuation of the audio object
        /// </summary>
        public float Attenuation
        {
            get { return SFMLSound.Attenuation; }
            set { SFMLSound.Attenuation = value; }
        }

        /// <summary>
        /// Is this Audio disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new Sound
        /// This plays an audio sound by loading it all in memorry first. For sound effects!
        /// </summary>
        /// <param name="sound">SFML Music to store</param>
        internal Sound(SoundBuffer sound)
        {
            //Store the sound, then the size in a MGF friendly format
            this.SFMLSound = new SFMLSound(sound.soundBuffer);
            this.Disposed = false;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Play the Sound Effect
        /// </summary>
        public void Play()
        {
            SFMLSound.Play();
        }

        /// <summary>
        /// Pause the Sound Effect
        /// </summary>
        public void Pause()
        {
            SFMLSound.Pause();
        }

        /// <summary>
        /// Stop the Sound Effect
        /// </summary>
        public void Stop()
        {
            SFMLSound.Stop();
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (SFMLSound != null)
            {
                SFMLSound.Dispose();
                SFMLSound = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Sound()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Music disposed?
        /// </summary>
        /// <returns>Is this Music disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
