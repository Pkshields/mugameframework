﻿using System;
using MuGameFramework.Components;
using MuGameFramework.Graphics.Shapes;
using SFMLText = SFML.Graphics.Text;
using SFMLShape = SFML.Graphics.Shape;
using SFMLRenderTarget = SFML.Graphics.RenderTarget;
using SFMLView = SFML.Graphics.View;
using SFMLVector2f = SFML.Window.Vector2f;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Enables Sprites, Strings and Shapes to be drawn on screen
    /// </summary>
    public class SpriteBatch : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// Default window attached to the SpriteBatch
        /// </summary>
        public Window DefaultWindow
        {
            get;
            private set;
        }

        /// <summary>
        /// Current RenderTarget
        /// </summary>
        private SFMLRenderTarget target;

        /// <summary>
        /// Sprite used to draw uncontained Textures
        /// </summary>
        private Sprite generalSprite;

        /// <summary>
        /// General Text used to draw strings on the screen
        /// </summary>
        private SFMLText generalText;

        /// <summary>
        /// Enable the disabling of drawing of objects off screen
        /// </summary>
        public bool FrustumCullingEnabled = true;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new instance of the SpriteBatch
        /// </summary>
        /// <param name="window">Window to set as it's default window</param>
        public SpriteBatch(Window window)
        {
            //Set the default window and the target to current default
            DefaultWindow = window;
            target = DefaultWindow.RenderWindow;

            //Initialize the general sprite and Text objects
            generalSprite = new Sprite();
            generalText = new SFMLText();
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Begin the SpriteBatch
        /// </summary>
        /// <param name="resetRenderTarget">Should I reset the render target? Probably.</param>
        internal void Begin(bool resetRenderTarget = true)
        {
            //reset the render target to the window
            if (resetRenderTarget)
                target = DefaultWindow.RenderWindow;

            //Clear the target window
            target.Clear();
        }

        /// <summary>
        /// Finish the SpriteBatch
        /// </summary>
        internal void End()
        {
            //Keep dislaying the default window
            DefaultWindow.Display();
        }

        /// <summary>
        /// Clear the current RenderTarget with a certain color
        /// </summary>
        /// <param name="color">Color to fill RenderTarget with</param>
        public void Clear(Color color)
        {
            //Reclear the target
            target.Clear(color.AsSFMLColor);
        }

        /// <summary>
        /// Change the current RenderTarget of this SpriteBatch
        /// </summary>
        /// <param name="target">New RenderTarget to set</param>
        public void ChangeRenderTarget(IRenderTarget target)
        {
            this.target = target.GetRenderTarget();
        }

        /// <summary>
        /// Change the current CameraView of this current RenderTarget
        /// </summary>
        /// <param name="view">New CameraView to set</param>
        public void SetCameraView(CameraView view)
        {
            target.SetView(view.SFMLView);
        }

        /// <summary>
        /// Check for Fustrum Culling - Don't draw any sprites that are not on screen to see
        /// </summary>
        /// <param name="positionRect">Rect of the sprite top check</param>
        /// <returns>If the sprite should draw or not</returns>
        private bool FrustumCulling(Rectangle positionRect)
        {
            //If Frustum Culling is enabled, the check that the sprite is on screen
            if (FrustumCullingEnabled)
            {
                //Convert the position that the Sprite is drawing on the View to it's real position on screen
                //Get the current view
                SFMLView currentView = target.GetView();

                //Get the position of the top left point on the view, then subtract this from the sprite position to
                // get the real screen position
                SFMLVector2f position = currentView.Center - (currentView.Size / 2);
                positionRect.X -= (int)position.X;
                positionRect.Y -= (int)position.Y;

                //Return the result of the check
                return !(positionRect.X + positionRect.Width < 0 ||
                         positionRect.Y + positionRect.Height < 0 ||
                         positionRect.X > DefaultWindow.WindowDimensions.Width ||
                         positionRect.Y > DefaultWindow.WindowDimensions.Height);
            }
            //Else the user has disabled it and it isn't needed for optimisation reasons, just draw
            else
                return true;
        }

                                                #endregion METHODS

                                                #region DRAW METHODS

        /// <summary>
        /// Draw a sprite onto the screen
        /// </summary>
        /// <param name="sprite">Sprite to draw on screen</param>
        public void Draw(Sprite sprite)
        {
            //Check for Frustum Culling
            if (FrustumCulling(sprite.GetFullRectangle()))
            {
                //Temp vector for later on
                Vector2? tempVector = null;

                //Check if the user has set any effects to apply
                if (sprite.Effects != SpriteEffects.None)
                {
                    //Yes! Set that temp vector we allocated earlier
                    tempVector = sprite.Scale;

                    //If we are flipping horizontally, negate the X axis scale so the image draws horizontally flipped
                    if ((sprite.Effects & SpriteEffects.FlipHorizontally) != 0)
                        sprite.Scale = new Vector2(sprite.Scale.X * -1, sprite.Scale.Y);

                    //If we are flipping vertically, negate the Y axis scale so the image draws vertically flipped
                    if ((sprite.Effects & SpriteEffects.FlipVertically) != 0)
                        sprite.Scale = new Vector2(sprite.Scale.X, sprite.Scale.Y * -1);
                }

                //Draw the image! On the RenderTarget!
                target.Draw(sprite.SFMLSprite);

                //If some effects have been applied, revert the changed values
                if (tempVector.HasValue)
                    sprite.Scale = tempVector.Value;
            }
        }

        /// <summary>
        /// Draw a Texture onto the screen
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="position">Position to draw it to on screne</param>
        /// <param name="color">Color to colour the texture</param>
        public void Draw(Texture2D texture, Vector2 position, Color color)
        {
            //Run the full Draw texture with all the arguments
            Draw(texture, position, color, new Rectangle(0, 0, texture.Size.Width, texture.Size.Height), Vector2.Zero, 0f, Vector2.One, SpriteEffects.None);
        }

        /// <summary>
        /// Draw a Texture onto the screen
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="position">Position to draw it to on screne</param>
        /// <param name="color">Color to colour the texture</param>
        /// <param name="sourceRectangle">Section of the sprite to draw on screen</param>
        public void Draw(Texture2D texture, Vector2 position, Color color, Rectangle sourceRectangle)
        {
            //Run the full Draw texture with all the arguments
            Draw(texture, position, color, sourceRectangle, Vector2.Zero, 0f, Vector2.One, SpriteEffects.None);
        }

        /// <summary>
        /// Draw a Texture onto the screen
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="position">Position to draw it to on screne</param>
        /// <param name="color">Color to colour the texture</param>
        /// <param name="sourceRectangle">Section of the sprite to draw on screen</param>
        /// <param name="origin">Origin of the sprite</param>
        /// <param name="rotation">Rotation of the sprite (in degrees)</param>
        public void Draw(Texture2D texture, Vector2 position, Color color, Rectangle sourceRectangle, Vector2 origin, float rotation)
        {
            //Run the full Draw texture with all the arguments
            Draw(texture, position, color, sourceRectangle, origin, rotation, Vector2.One, SpriteEffects.None);
        }

        /// <summary>
        /// Draw a Texture onto the screen
        /// </summary>
        /// <param name="texture">Texture to draw</param>
        /// <param name="position">Position to draw it to on screne</param>
        /// <param name="color">Color to colour the texture</param>
        /// <param name="sourceRectangle">Section of the sprite to draw on screen</param>
        /// <param name="origin">Origin of the sprite</param>
        /// <param name="rotation">Rotation of the sprite (in degrees)</param>
        /// <param name="scale">Scale of the sprite</param>
        /// <param name="effects">Effects to apply to the sprite</param>
        public void Draw(Texture2D texture, Vector2 position, Color color, Rectangle sourceRectangle, Vector2 origin, float rotation, 
            Vector2 scale, SpriteEffects effects)
        {
            //Fill out the General Sprite using the provided texture
            generalSprite.Initialize(texture);

            //Then the proivided settings
            generalSprite.Position = position;
            generalSprite.Color = color;
            generalSprite.SourceRectangle = sourceRectangle;
            generalSprite.Origin = origin;
            generalSprite.Rotation = rotation;
            generalSprite.Scale = scale;
            generalSprite.Effects = effects;

            //Then run the reusable Draw method with the general sprite here
            Draw(generalSprite);
        }

        /// <summary>
        /// Draw a line of text onto the screen
        /// </summary>
        /// <param name="font">Font to draw with</param>
        /// <param name="text">Text to draw</param>
        /// <param name="position">Position to draw it to on screen</param>
        /// <param name="color">Color to colour the text</param>
        public void DrawString(Font font, string text, Vector2 position, Color color)
        {
            //Use the overloaded DrawString to draw a string
            DrawString(font, text, position, color, Vector2.Zero, 0f, Vector2.One, 30, FontStyle.Regular);
        }

        /// <summary>
        /// Draw a line of text onto the screen
        /// </summary>
        /// <param name="font">Font to draw with</param>
        /// <param name="text">Text to draw</param>
        /// <param name="position">Position to draw it to on screen</param>
        /// <param name="color">Color to colour the text</param>
        /// <param name="origin">Origin of the text</param>
        /// <param name="rotation">Rotation of the text (in degrees)</param>
        /// <param name="scale">Scale of the text</param>
        /// <param name="characterSize">Size of the characters</param>
        /// <param name="style">String styling</param>
        public void DrawString(Font font, string text, Vector2 position, Color color, Vector2 origin, float rotation, Vector2 scale, 
            uint characterSize, FontStyle style)
        {
            //Calculate the Frustum Culling check rect
            Size checkSize = font.MeasureString(text, characterSize);
            Rectangle checkRect = new Rectangle((int)position.X, (int)position.Y, checkSize.Width, checkSize.Height);

            //Then check the Frustum Culling
            if (FrustumCulling(checkRect))
            {
                //Passed!
                //Set up the text to draw
                generalText.DisplayedString = text;
                generalText.Font = font.font;
                generalText.Position = position.AsSFMLVector2f;
                generalText.Color = color.AsSFMLColor;
                generalText.Origin = origin.AsSFMLVector2f;
                generalText.Rotation = rotation;
                generalText.Scale = scale.AsSFMLVector2f;
                generalText.Style = (SFMLText.Styles)style;
                generalText.CharacterSize = characterSize;

                //Draw the new general string on screen
                target.Draw(generalText);
            }
        }

        /// <summary>
        /// Draw a Shape onto the screen
        /// </summary>
        /// <param name="shape">Shape to draw on screen</param>
        public void DrawShape(Shape shape)
        {
            //Check for Frustum Culling
            if (FrustumCulling(shape.GetFullRectangle()))
            {
                //Temp vector for later on
                Vector2? tempVector = null;

                //Check if the user has set any effects to apply
                if (shape.Effects != SpriteEffects.None)
                {
                    //Yes! Set that temp vector we allocated earlier
                    tempVector = shape.Scale;

                    //If we are flipping horizontally, negate the X axis scale so the shape draws horizontally flipped
                    if ((shape.Effects & SpriteEffects.FlipHorizontally) != 0)
                        shape.Scale = new Vector2(shape.Scale.X * -1, shape.Scale.Y);

                    //If we are flipping vertically, negate the Y axis scale so the shape draws vertically flipped
                    if ((shape.Effects & SpriteEffects.FlipVertically) != 0)
                        shape.Scale = new Vector2(shape.Scale.X, shape.Scale.Y * -1);
                }

                //Draw the shape! On the RenderTarget!
                target.Draw(shape.genShape);

                //If some effects have been applied, revert the changed values
                if (tempVector.HasValue)
                    shape.Scale = tempVector.Value;
            }
        }

                                                #endregion DRAW METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (generalSprite != null)
            {
                generalSprite.Dispose();
                generalSprite = null;
            }

            if (generalText != null)
            {
                generalText.Dispose();
                generalText = null;
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~SpriteBatch()
        {
            Dispose(false);
        }

                                                #endregion INTERFACE METHODS
    }
}
