﻿using System;
using MuGameFramework.Components;
using MuGameFramework.Utilities;
using SFMLSprite = SFML.Graphics.Sprite;
using SFMLIntRect = SFML.Graphics.IntRect;
using SFMLColor = SFML.Graphics.Color;
using SFMLVector2f = SFML.Window.Vector2f;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Class representing a sprite drawn on screen with all generic properties and methods
    /// </summary>
    public class Sprite : IDisposable
    {
                                                #region PROPERTIES
        
        /// <summary>
        /// SFML sprite
        /// </summary>
        internal SFMLSprite SFMLSprite;

        /// <summary>
        /// Position of the sprite on screen
        /// </summary>
        public Vector2 Position
        {
            get { return new Vector2(SFMLSprite.Position.X, SFMLSprite.Position.Y); }
            set { SFMLSprite.Position = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Origin to draw the sprite from
        /// </summary>
        public Vector2 Origin
        {
            get { return new Vector2(SFMLSprite.Origin.X, SFMLSprite.Origin.Y); }
            set { SFMLSprite.Origin = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Color of the sprite
        /// </summary>
        public Color Color
        {
            get { return new Color(SFMLSprite.Color.R, SFMLSprite.Color.G, SFMLSprite.Color.B, SFMLSprite.Color.A); }
            set { SFMLSprite.Color = value.AsSFMLColor; }
        }

        /// <summary>
        /// Rotation of the sprite in degrees
        /// </summary>
        public float Rotation
        {
            get { return SFMLSprite.Rotation; }
            set { SFMLSprite.Rotation = value; }
        }

        /// <summary>
        /// Scale of the sprite
        /// </summary>
        public Vector2 Scale
        {
            get { return new Vector2(SFMLSprite.Scale.X, SFMLSprite.Scale.Y); }
            set { SFMLSprite.Scale = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Rectangle of the sprite to draw on screen
        /// </summary>
        public Rectangle SourceRectangle
        {
            get { return new Rectangle(SFMLSprite.TextureRect.Left, SFMLSprite.TextureRect.Top, SFMLSprite.TextureRect.Width, SFMLSprite.TextureRect.Height); }
            set { SFMLSprite.TextureRect = value.AsSFMLIntRect; }
        }

        /// <summary>
        /// Effects to apply to the sprite
        /// </summary>
        public SpriteEffects Effects = SpriteEffects.None;

        /// <summary>
        /// Full size of the sprite
        /// </summary>
        public Size FullSize
        {
            get { return new Size(FullRectangle.Width, FullRectangle.Height); }
        }

        /// <summary>
        /// Full rectangle of the sprite
        /// </summary>
        public Rectangle FullRectangle
        {
            get;
            private set;
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new sprite without a Texture2D for SpriteBatch
        /// </summary>
        internal Sprite()
        { }

        /// <summary>
        /// Create a new sprite from a Texture2D
        /// </summary>
        /// <param name="texture">Texture to create the sprite from</param>
        public Sprite(Texture2D texture)
        {
            //Run initialization
            Initialize(texture);
        }

        /// <summary>
        /// Reinitialize this sprite using another Texture2D
        /// </summary>
        /// <param name="texture">Texture to create the sprite from</param>
        public void Initialize(Texture2D texture)
        {
            //Initialize the SFMLSprite
            SFMLSprite = new SFMLSprite(texture.texture);
            SFMLSprite.TextureRect = new SFMLIntRect(0, 0, texture.Size.Width, texture.Size.Height);
            SFMLSprite.Color = SFMLColor.White;
            SFMLSprite.Position = new SFMLVector2f(0, 0);

            //Then store the full rectangle for reference
            FullRectangle = new Rectangle(0, 0, texture.Size.Width, texture.Size.Height);
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Cache fix: Sprite from a Texture2D cast
        /// </summary>
        /// <param name="value">Texture2D to get a Sprite for</param>
        /// <returns>New Sprite</returns>
        public static explicit operator Sprite(Texture2D value)
        {
            return new Sprite(value);
        }

                                                #endregion OPERATOR METHODS

                                                #region METHODS

        /// <summary>
        /// Get the simple positioned rectangle for this sprite. 
        /// Neither Scale, Rotation or SpriteEffects are applied
        /// </summary>
        /// <returns>Simple positioned rectangle for this sprite</returns>
        public Rectangle GetRectangle()
        {
            return new Rectangle((int)(Position.X - Origin.X),
                                 (int)(Position.X - Origin.X),
                                 (int)(SFMLSprite.TextureRect.Width),
                                 (int)(SFMLSprite.TextureRect.Height));
        }

        /// <summary>
        /// Get the simple positioned rectangle for this sprite. 
        /// Scale is applied, but Rotation and SpriteEffects are not
        /// </summary>
        /// <returns>Simple positioned rectangle w. Sca;e</returns>
        public Rectangle GetScaledRectangle()
        {
            return new Rectangle((int)(Position.X - Origin.X),
                                 (int)(Position.Y - Origin.Y),
                                 (int)(SFMLSprite.TextureRect.Width * Scale.X),
                                 (int)(SFMLSprite.TextureRect.Height * Scale.Y));
        }

        /// <summary>
        /// Get the fully modified positioned rectangle for this sprite. 
        /// Scale, Rotation and SpriteEffects are applied
        /// </summary>
        /// <returns>Fully modified positioned rectangle</returns>
        public Rectangle GetFullRectangle()
        {
            //Firstly, get the scaled rectangle
            Rectangle fullRect = GetScaledRectangle();

            //If the sprite has been rotated, modify it accordingly
            if (SFMLSprite.Rotation != 0f)
            {
                //Convert the rotation to radians 
                float rotation = MathHelper.DegreesToRadians(SFMLSprite.Rotation);

                //Set up the 4 points of the current rectangle in Point structs for rotating
                Vector2[] fourPoints = { new Vector2(fullRect.X, fullRect.Y),                                       //Top Left
                                         new Vector2(fullRect.X, fullRect.Y + fullRect.Height),                     //Top Right
                                         new Vector2(fullRect.X + fullRect.Width, fullRect.Y),                      //Bottom Left
                                         new Vector2(fullRect.X + fullRect.Width, fullRect.Y + fullRect.Height) };  //Bottom Right

                //Rotate the four points around the origin point by the rotation angle
                for (int i = 0; i < fourPoints.Length; i++)
                    fourPoints[i] = MathHelper.RotatePoint(fourPoints[i], Position, rotation);

                //Set some (dummy) initial values to make the following loops work
                fullRect.X = (int)fourPoints[0].X;
                fullRect.Y = (int)fourPoints[0].Y;
                fullRect.Width = 0;
                fullRect.Height = 0;

                //Firstly, loop through the points, finding the top left coordinates
                for(int i = 1; i < fourPoints.Length; i++)
                {
                    if (fourPoints[i].X < fullRect.X) fullRect.X  = (int)fourPoints[i].X;
                    if (fourPoints[i].Y < fullRect.Y) fullRect.Y = (int)fourPoints[i].Y;
                }

                //Then, once that is calculated, calculate the width and height
                for (int i = 0; i < fourPoints.Length; i++)
                {
                    if (fourPoints[i].X - fullRect.X > fullRect.Width) fullRect.Width = (int)fourPoints[i].X - fullRect.X;
                    if (fourPoints[i].Y - fullRect.Y > fullRect.Height) fullRect.Height = (int)fourPoints[i].Y - fullRect.Y;
                }

                //Calculation done! Values whould be in the FullRect
            }

            //If the sprite has effects via SpriteEffects applied to it, modify accordingly
            if (Effects != SpriteEffects.None)
            {
                //If the sprite is flipped horizontally, then move the X coordinate up accordingly
                // depending on the height of the sprite and the position of the origin 
                if ((Effects & SpriteEffects.FlipHorizontally) == SpriteEffects.FlipHorizontally)
                    fullRect.X -= (int)(Math.Abs((fullRect.Width / 2f) - SFMLSprite.Origin.X) + (fullRect.Width / 2f));

                //Same to the vertical flip
                if ((Effects & SpriteEffects.FlipVertically) == SpriteEffects.FlipVertically)
                    fullRect.Y -= (int)(Math.Abs((fullRect.Height / 2f) - SFMLSprite.Origin.Y) + (fullRect.Height / 2f));
            }

            //Return the true modified rectangle
            return fullRect;
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (SFMLSprite != null)
            {
                SFMLSprite.Dispose();
                SFMLSprite = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Sprite()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        /// <returns>Is this Texture disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}