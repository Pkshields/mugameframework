﻿using System;
using MuGameFramework.Components;
using SFMLTexture = SFML.Graphics.Texture;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Represents a 2D texture stored in memory
    /// </summary>
    public class Texture2D : IContent
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML texture used to store graphics
        /// </summary>
        internal SFMLTexture texture
        {
            get;
            private set;
        }

        /// <summary>
        /// Set if the Texture2D should repeat or not
        /// </summary>
        public bool Repeated
        {
            get { return texture.Repeated; }
            set { texture.Repeated = value; }
        }

        /// <summary>
        /// Gets the size fo the texture
        /// </summary>
        public Size Size
        {
            get;
            private set;
        }

        /// <summary>
        /// Enable or disable the smooth filter
        /// </summary>
        public bool Smooth
        {
            get { return texture.Smooth; }
            set { texture.Smooth = value; }
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTOR

        /// <summary>
        /// Create a new Texture2D (Used only in ContentManager)
        /// </summary>
        internal Texture2D()
        {
            //Content is not loaded yet, let on that the object is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Create a new Texture2D
        /// </summary>
        /// <param name="fileToLoad">SFML texture to store</param>
        internal Texture2D(string fileToLoad)
        {
            //Store the texture, then the size in a MGF friendly format
            texture = new SFMLTexture(fileToLoad);
            Size = new Size((int)texture.Size.X, (int)texture.Size.Y);
            this.Disposed = false;
        }

        /// <summary>
        /// Create a new Texture2D
        /// </summary>
        /// <param name="texture">SFML texture to store</param>
        internal Texture2D(SFMLTexture texture)
        {
            //Store the texture, then the size in a MGF friendly format
            this.texture = texture;
            Size = new Size((int)texture.Size.X, (int)texture.Size.Y);
            this.Disposed = false;
        }

        /// <summary>
        /// Load a file into this object (if file is not already loaded into it)
        /// </summary>
        /// <param name="fileToLoad">Location of file to load</param>
        void IContent.LoadContent(string fileToLoad)
        {
            //Store the texture, then the size in a MGF friendly format
            texture = new SFMLTexture(fileToLoad);
            Size = new Size((int)texture.Size.X, (int)texture.Size.Y);
            this.Disposed = false;
        }

                                                #endregion CONSTRUCTOR

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (texture != null)
            {
                texture.Dispose();
                texture = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Texture2D()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        /// <returns>Is this Texture disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
