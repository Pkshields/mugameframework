﻿using System;
using MuGameFramework.Components;
using SFMLFont = SFML.Graphics.Font;
using SFMLText = SFML.Graphics.Text;
using SFMLFloatRect = SFML.Graphics.FloatRect;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Representing a font in memory for use for drawing text on screen
    /// </summary>
    public class Font : IContent
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML font object that we are wrapping around
        /// </summary>
        internal SFMLFont font
        {
            get;
            private set;
        }

        /// <summary>
        /// Get the default font for basic text rendering
        /// </summary>
        public readonly static Font DefaultFont = new Font(SFMLFont.DefaultFont);

        /// <summary>
        /// Default size at which the strings are drawn at
        /// </summary>
        public readonly static uint DefaultSize = 30;

        /// <summary>
        /// SFML String used for measuring strings
        /// </summary>
        private static SFMLText measureString = new SFMLText();

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new Font object (Used only in ContentManager)
        /// </summary>
        internal Font()
        {
            //Content is not loaded yet, let on that the object is disposed
            Disposed = true;
        }

        /// <summary>
        /// Create a new Font object
        /// </summary>
        /// <param name="fileToLoad">Font to load</param>
        internal Font(string fileToLoad)
        {
            this.font = new SFMLFont(fileToLoad);
            Disposed = false;
        }

        /// <summary>
        /// Create a new Font object
        /// </summary>
        /// <param name="font">SFML font loaded</param>
        internal Font(SFMLFont font)
        {
            this.font = font;
            Disposed = false;
        }

        /// <summary>
        /// Load a file into this object (if file is not already loaded into it)
        /// </summary>
        /// <param name="fileToLoad">Location of file to load</param>
        void IContent.LoadContent(string fileToLoad)
        {
            this.font = new SFMLFont(fileToLoad);
            Disposed = false;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the kerning offset of two glyphs
        /// </summary>
        /// <param name="first">Unicode code point of the first character</param>
        /// <param name="second">Unicode code point of the second character</param>
        /// <param name="characterSize">Reference character size</param>
        /// <returns>Kerning of the characters</returns>
        public int GetKerning(uint first, uint second, uint characterSize)
        {
            return font.GetKerning(first, second, characterSize);
        }

        /// <summary>
        /// Get the line spacing. Vertical offset to apply between two consecutive lines of text
        /// </summary>
        /// <param name="characterSize">Reference character size</param>
        /// <returns>Reference character size</returns>
        public int GetLineSpacing(uint characterSize)
        {
            return font.GetLineSpacing(characterSize);
        }

        /// <summary>
        /// Retrieve the texture containing the loaded glyphs of a certain size.
        /// </summary>
        /// <param name="characterSize"></param>
        /// <returns></returns>
        public Texture2D GetTexture(uint characterSize)
        {
            return new Texture2D(font.GetTexture(characterSize));
        }

        /// <summary>
        /// Measure a string using this font type
        /// </summary>
        /// <param name="toMeasure">String to measure</param>
        /// <param name="fontSize">Size of the font</param>
        /// <returns>Size of the string</returns>
        public Size MeasureString(string toMeasure, uint fontSize = 30)
        {
            //Setup the general font with these settings to measure
            measureString.Font = this.font;
            measureString.DisplayedString = toMeasure;
            measureString.CharacterSize = fontSize;

            //Let SFML calculate the size of the string
            SFMLFloatRect bounds = measureString.GetGlobalBounds();

            //Send it back as a MGF Size object
            return new Size((int)bounds.Width, (int)bounds.Height);
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (font != null)
            {
                font.Dispose();
                font = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Font()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Font disposed?
        /// </summary>
        /// <returns>Is this Font disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
