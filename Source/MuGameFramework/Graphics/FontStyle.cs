﻿using System;
using SFML.Graphics;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Different styles supported by DrawString
    /// </summary>
    [Flags]
    public enum FontStyle
    {
        Bold = Text.Styles.Bold,
        Italic = Text.Styles.Italic,
        Regular = Text.Styles.Regular,
        Underlined = Text.Styles.Underlined
    }
}
