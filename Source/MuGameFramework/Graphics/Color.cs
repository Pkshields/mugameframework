﻿using System;
using SFMLColor = SFML.Graphics.Color;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Represents an RGBA color object
    /// </summary>
    public struct Color
    {
                                                #region PROPERIES

        /// <summary>
        /// Red value of this Color object
        /// </summary>
        public byte R;

        /// <summary>
        /// Green value of this Color object
        /// </summary>
        public byte G;

        /// <summary>
        /// Blue value of this Color object
        /// </summary>
        public byte B;

        /// <summary>
        /// Alpha value of this Color object
        /// </summary>
        public byte A;

                                                #endregion PROPERTIES

                                                #region IMPLICIT PROPERTIES

        /// <summary>
        /// Get/Set this Color as a SFMLColor
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal SFMLColor AsSFMLColor
        {
            get { return new SFMLColor(R, G, B, A); }
            set { R = value.R; G = value.G; B = value.B; A = value.A; }
        }

                                                #endregion IMPLICIT PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of the Color object
        /// </summary>
        /// <param name="r">Red color (between 0 and 255)</param>
        /// <param name="g">Green color (between 0 and 255)</param>
        /// <param name="b">Blue color (between 0 and 255)</param>
        /// <param name="alpha">Alpha value of this color</param>
        public Color(byte r, byte g, byte b, byte alpha)
        {
            this.R = r;
            this.G = g;
            this.B = b;
            this.A = alpha;
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Print out the values of a Color as a string
        /// </summary>
        /// <param name="value">Color to print out</param>
        /// <returns>String of thus Color</returns>
        public static implicit operator string(Color value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Compare two Colors for equality
        /// </summary>
        /// <param name="a">Color A to check</param>
        /// <param name="b">Color B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(Color a, Color b)
        {
            //Check that either Color is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.R == b.R && a.G == b.G && a.B == b.B && a.A == b.A);
        }

        /// <summary>
        /// Compare two Colors for inequality
        /// </summary>
        /// <param name="a">Color A to check</param>
        /// <param name="b">Color B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(Color a, Color b)
        {
            //Check that either vector2 is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.R != b.R && a.G != b.G && a.B != b.B && a.A != b.A);
        }

                                                #endregion OPERATOR METHODS

                                                #region OVERRIDE METHODS

        /// <summary>
        /// Compare a Color with this for equality
        /// </summary>
        /// <param name="obj">Color to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is Color)
                return (this == (Color)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this object
                hash = hash * 23 + R.GetHashCode();
                hash = hash * 23 + G.GetHashCode();
                hash = hash * 23 + B.GetHashCode();
                hash = hash * 23 + A.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Print out the values of a Color as a string
        /// </summary>
        /// <returns>String of thus Vector2</returns>
        public override string ToString()
        {
            return "(" + R + ", " + G + ", " + B + ", " + A + ")";
        }

                                                #endregion OVERRIDE METHODS
    }
}
