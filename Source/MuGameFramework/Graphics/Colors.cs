﻿using System;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// A bunch of basic defaults colors for your use
    /// </summary>
    public static class Colors
    {
        /// <summary>
        /// Return a Color object "AliceBlue" with the value (240, 248, 255, 255)
        /// </summary>
        public static Color AliceBlue
        {
            get { return new Color(240, 248, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "AntiqueWhite" with the value (250, 235, 215, 255)
        /// </summary>
        public static Color AntiqueWhite
        {
            get { return new Color(250, 235, 215, 255); }
        }

        /// <summary>
        /// Return a Color object "Aqua" with the value (0, 255, 255, 255)
        /// </summary>
        public static Color Aqua
        {
            get { return new Color(0, 255, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Aquamarine" with the value (127, 255, 212, 255)
        /// </summary>
        public static Color Aquamarine
        {
            get { return new Color(127, 255, 212, 255); }
        }

        /// <summary>
        /// Return a Color object "Azure" with the value (240, 255, 255, 255)
        /// </summary>
        public static Color Azure
        {
            get { return new Color(240, 255, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Beige" with the value (245, 245, 220, 255)
        /// </summary>
        public static Color Beige
        {
            get { return new Color(245, 245, 220, 255); }
        }

        /// <summary>
        /// Return a Color object "Bisque" with the value (255, 228, 196, 255)
        /// </summary>
        public static Color Bisque
        {
            get { return new Color(255, 228, 196, 255); }
        }

        /// <summary>
        /// Return a Color object "Black" with the value (0, 0, 0, 255)
        /// </summary>
        public static Color Black
        {
            get { return new Color(0, 0, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "BlanchedAlmond" with the value (255, 235, 205, 255)
        /// </summary>
        public static Color BlanchedAlmond
        {
            get { return new Color(255, 235, 205, 255); }
        }

        /// <summary>
        /// Return a Color object "Blue" with the value (0, 0, 255, 255)
        /// </summary>
        public static Color Blue
        {
            get { return new Color(0, 0, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "BlueViolet" with the value (138, 43, 226, 255)
        /// </summary>
        public static Color BlueViolet
        {
            get { return new Color(138, 43, 226, 255); }
        }

        /// <summary>
        /// Return a Color object "Brown" with the value (165, 42, 42, 255)
        /// </summary>
        public static Color Brown
        {
            get { return new Color(165, 42, 42, 255); }
        }

        /// <summary>
        /// Return a Color object "BurlyWood" with the value (222, 184, 135, 255)
        /// </summary>
        public static Color BurlyWood
        {
            get { return new Color(222, 184, 135, 255); }
        }

        /// <summary>
        /// Return a Color object "CadetBlue" with the value (95, 158, 160, 255)
        /// </summary>
        public static Color CadetBlue
        {
            get { return new Color(95, 158, 160, 255); }
        }

        /// <summary>
        /// Return a Color object "Chartreuse" with the value (127, 255, 0, 255)
        /// </summary>
        public static Color Chartreuse
        {
            get { return new Color(127, 255, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "Chocolate" with the value (210, 105, 30, 255)
        /// </summary>
        public static Color Chocolate
        {
            get { return new Color(210, 105, 30, 255); }
        }

        /// <summary>
        /// Return a Color object "Coral" with the value (255, 127, 80, 255)
        /// </summary>
        public static Color Coral
        {
            get { return new Color(255, 127, 80, 255); }
        }

        /// <summary>
        /// Return a Color object "CornflowerBlue" with the value (100, 149, 237, 255)
        /// </summary>
        public static Color CornflowerBlue
        {
            get { return new Color(100, 149, 237, 255); }
        }

        /// <summary>
        /// Return a Color object "Cornsilk" with the value (255, 248, 220, 255)
        /// </summary>
        public static Color Cornsilk
        {
            get { return new Color(255, 248, 220, 255); }
        }

        /// <summary>
        /// Return a Color object "Crimson" with the value (220, 20, 60, 255)
        /// </summary>
        public static Color Crimson
        {
            get { return new Color(220, 20, 60, 255); }
        }

        /// <summary>
        /// Return a Color object "Cyan" with the value (0, 255, 255, 255)
        /// </summary>
        public static Color Cyan
        {
            get { return new Color(0, 255, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkBlue" with the value (0, 0, 139, 255)
        /// </summary>
        public static Color DarkBlue
        {
            get { return new Color(0, 0, 139, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkCyan" with the value (0, 139, 139, 255)
        /// </summary>
        public static Color DarkCyan
        {
            get { return new Color(0, 139, 139, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkGoldenrod" with the value (184, 134, 11, 255)
        /// </summary>
        public static Color DarkGoldenrod
        {
            get { return new Color(184, 134, 11, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkGray" with the value (169, 169, 169, 255)
        /// </summary>
        public static Color DarkGray
        {
            get { return new Color(169, 169, 169, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkGreen" with the value (0, 100, 0, 255)
        /// </summary>
        public static Color DarkGreen
        {
            get { return new Color(0, 100, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkKhaki" with the value (189, 183, 107, 255)
        /// </summary>
        public static Color DarkKhaki
        {
            get { return new Color(189, 183, 107, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkMagenta" with the value (139, 0, 139, 255)
        /// </summary>
        public static Color DarkMagenta
        {
            get { return new Color(139, 0, 139, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkOliveGreen" with the value (85, 107, 47, 255)
        /// </summary>
        public static Color DarkOliveGreen
        {
            get { return new Color(85, 107, 47, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkOrange" with the value (255, 140, 0, 255)
        /// </summary>
        public static Color DarkOrange
        {
            get { return new Color(255, 140, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkOrchid" with the value (153, 50, 204, 255)
        /// </summary>
        public static Color DarkOrchid
        {
            get { return new Color(153, 50, 204, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkRed" with the value (139, 0, 0, 255)
        /// </summary>
        public static Color DarkRed
        {
            get { return new Color(139, 0, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkSalmon" with the value (233, 150, 122, 255)
        /// </summary>
        public static Color DarkSalmon
        {
            get { return new Color(233, 150, 122, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkSeaGreen" with the value (143, 188, 139, 255)
        /// </summary>
        public static Color DarkSeaGreen
        {
            get { return new Color(143, 188, 139, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkSlateBlue" with the value (72, 61, 139, 255)
        /// </summary>
        public static Color DarkSlateBlue
        {
            get { return new Color(72, 61, 139, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkSlateGray" with the value (47, 79, 79, 255)
        /// </summary>
        public static Color DarkSlateGray
        {
            get { return new Color(47, 79, 79, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkTurquoise" with the value (0, 206, 209, 255)
        /// </summary>
        public static Color DarkTurquoise
        {
            get { return new Color(0, 206, 209, 255); }
        }

        /// <summary>
        /// Return a Color object "DarkViolet" with the value (148, 0, 211, 255)
        /// </summary>
        public static Color DarkViolet
        {
            get { return new Color(148, 0, 211, 255); }
        }

        /// <summary>
        /// Return a Color object "DeepPink" with the value (255, 20, 147, 255)
        /// </summary>
        public static Color DeepPink
        {
            get { return new Color(255, 20, 147, 255); }
        }

        /// <summary>
        /// Return a Color object "DeepSkyBlue" with the value (0, 191, 255, 255)
        /// </summary>
        public static Color DeepSkyBlue
        {
            get { return new Color(0, 191, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "DimGray" with the value (105, 105, 105, 255)
        /// </summary>
        public static Color DimGray
        {
            get { return new Color(105, 105, 105, 255); }
        }

        /// <summary>
        /// Return a Color object "DodgerBlue" with the value (30, 144, 255, 255)
        /// </summary>
        public static Color DodgerBlue
        {
            get { return new Color(30, 144, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Firebrick" with the value (178, 34, 34, 255)
        /// </summary>
        public static Color Firebrick
        {
            get { return new Color(178, 34, 34, 255); }
        }

        /// <summary>
        /// Return a Color object "FloralWhite" with the value (255, 250, 240, 255)
        /// </summary>
        public static Color FloralWhite
        {
            get { return new Color(255, 250, 240, 255); }
        }

        /// <summary>
        /// Return a Color object "ForestGreen" with the value (34, 139, 34, 255)
        /// </summary>
        public static Color ForestGreen
        {
            get { return new Color(34, 139, 34, 255); }
        }

        /// <summary>
        /// Return a Color object "Fuchsia" with the value (255, 0, 255, 255)
        /// </summary>
        public static Color Fuchsia
        {
            get { return new Color(255, 0, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Gainsboro" with the value (220, 220, 220, 255)
        /// </summary>
        public static Color Gainsboro
        {
            get { return new Color(220, 220, 220, 255); }
        }

        /// <summary>
        /// Return a Color object "GhostWhite" with the value (248, 248, 255, 255)
        /// </summary>
        public static Color GhostWhite
        {
            get { return new Color(248, 248, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Gold" with the value (255, 215, 0, 255)
        /// </summary>
        public static Color Gold
        {
            get { return new Color(255, 215, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "Goldenrod" with the value (218, 165, 32, 255)
        /// </summary>
        public static Color Goldenrod
        {
            get { return new Color(218, 165, 32, 255); }
        }

        /// <summary>
        /// Return a Color object "Gray" with the value (128, 128, 128, 255)
        /// </summary>
        public static Color Gray
        {
            get { return new Color(128, 128, 128, 255); }
        }

        /// <summary>
        /// Return a Color object "Green" with the value (0, 255, 0, 255)
        /// </summary>
        public static Color Green
        {
            get { return new Color(0, 255, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "GreenYellow" with the value (173, 255, 47, 255)
        /// </summary>
        public static Color GreenYellow
        {
            get { return new Color(173, 255, 47, 255); }
        }

        /// <summary>
        /// Return a Color object "Honeydew" with the value (240, 255, 240, 255)
        /// </summary>
        public static Color Honeydew
        {
            get { return new Color(240, 255, 240, 255); }
        }

        /// <summary>
        /// Return a Color object "HotPink" with the value (255, 105, 180, 255)
        /// </summary>
        public static Color HotPink
        {
            get { return new Color(255, 105, 180, 255); }
        }

        /// <summary>
        /// Return a Color object "IndianRed" with the value (205, 92, 92, 255)
        /// </summary>
        public static Color IndianRed
        {
            get { return new Color(205, 92, 92, 255); }
        }

        /// <summary>
        /// Return a Color object "Indigo" with the value (75, 0, 130, 255)
        /// </summary>
        public static Color Indigo
        {
            get { return new Color(75, 0, 130, 255); }
        }

        /// <summary>
        /// Return a Color object "Ivory" with the value (255, 255, 240, 255)
        /// </summary>
        public static Color Ivory
        {
            get { return new Color(255, 255, 240, 255); }
        }

        /// <summary>
        /// Return a Color object "Khaki" with the value (240, 230, 140, 255)
        /// </summary>
        public static Color Khaki
        {
            get { return new Color(240, 230, 140, 255); }
        }

        /// <summary>
        /// Return a Color object "Lavender" with the value (230, 230, 250, 255)
        /// </summary>
        public static Color Lavender
        {
            get { return new Color(230, 230, 250, 255); }
        }

        /// <summary>
        /// Return a Color object "LavenderBlush" with the value (255, 240, 245, 255)
        /// </summary>
        public static Color LavenderBlush
        {
            get { return new Color(255, 240, 245, 255); }
        }

        /// <summary>
        /// Return a Color object "LawnGreen" with the value (124, 252, 0, 255)
        /// </summary>
        public static Color LawnGreen
        {
            get { return new Color(124, 252, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "LemonChiffon" with the value (255, 250, 205, 255)
        /// </summary>
        public static Color LemonChiffon
        {
            get { return new Color(255, 250, 205, 255); }
        }

        /// <summary>
        /// Return a Color object "LightBlue" with the value (173, 216, 230, 255)
        /// </summary>
        public static Color LightBlue
        {
            get { return new Color(173, 216, 230, 255); }
        }

        /// <summary>
        /// Return a Color object "LightCoral" with the value (240, 128, 128, 255)
        /// </summary>
        public static Color LightCoral
        {
            get { return new Color(240, 128, 128, 255); }
        }

        /// <summary>
        /// Return a Color object "LightCyan" with the value (224, 255, 255, 255)
        /// </summary>
        public static Color LightCyan
        {
            get { return new Color(224, 255, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "LightGoldenrodYellow" with the value (250, 250, 210, 255)
        /// </summary>
        public static Color LightGoldenrodYellow
        {
            get { return new Color(250, 250, 210, 255); }
        }

        /// <summary>
        /// Return a Color object "LightGray" with the value (211, 211, 211, 255)
        /// </summary>
        public static Color LightGray
        {
            get { return new Color(211, 211, 211, 255); }
        }

        /// <summary>
        /// Return a Color object "LightGreen" with the value (144, 238, 144, 255)
        /// </summary>
        public static Color LightGreen
        {
            get { return new Color(144, 238, 144, 255); }
        }

        /// <summary>
        /// Return a Color object "LightPink" with the value (255, 182, 193, 255)
        /// </summary>
        public static Color LightPink
        {
            get { return new Color(255, 182, 193, 255); }
        }

        /// <summary>
        /// Return a Color object "LightSalmon" with the value (255, 160, 122, 255)
        /// </summary>
        public static Color LightSalmon
        {
            get { return new Color(255, 160, 122, 255); }
        }

        /// <summary>
        /// Return a Color object "LightSeaGreen" with the value (32, 178, 170, 255)
        /// </summary>
        public static Color LightSeaGreen
        {
            get { return new Color(32, 178, 170, 255); }
        }

        /// <summary>
        /// Return a Color object "LightSkyBlue" with the value (135, 206, 250, 255)
        /// </summary>
        public static Color LightSkyBlue
        {
            get { return new Color(135, 206, 250, 255); }
        }

        /// <summary>
        /// Return a Color object "LightSlateGray" with the value (119, 136, 153, 255)
        /// </summary>
        public static Color LightSlateGray
        {
            get { return new Color(119, 136, 153, 255); }
        }

        /// <summary>
        /// Return a Color object "LightSteelBlue" with the value (176, 196, 222, 255)
        /// </summary>
        public static Color LightSteelBlue
        {
            get { return new Color(176, 196, 222, 255); }
        }

        /// <summary>
        /// Return a Color object "LightYellow" with the value (255, 255, 224, 255)
        /// </summary>
        public static Color LightYellow
        {
            get { return new Color(255, 255, 224, 255); }
        }

        /// <summary>
        /// Return a Color object "Lime" with the value (0, 255, 0, 255)
        /// </summary>
        public static Color Lime
        {
            get { return new Color(0, 255, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "LimeGreen" with the value (50, 205, 50, 255)
        /// </summary>
        public static Color LimeGreen
        {
            get { return new Color(50, 205, 50, 255); }
        }

        /// <summary>
        /// Return a Color object "Linen" with the value (250, 240, 230, 255)
        /// </summary>
        public static Color Linen
        {
            get { return new Color(250, 240, 230, 255); }
        }

        /// <summary>
        /// Return a Color object "Magenta" with the value (255, 0, 255, 255)
        /// </summary>
        public static Color Magenta
        {
            get { return new Color(255, 0, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "Maroon" with the value (128, 0, 0, 255)
        /// </summary>
        public static Color Maroon
        {
            get { return new Color(128, 0, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumAquamarine" with the value (102, 205, 170, 255)
        /// </summary>
        public static Color MediumAquamarine
        {
            get { return new Color(102, 205, 170, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumBlue" with the value (0, 0, 205, 255)
        /// </summary>
        public static Color MediumBlue
        {
            get { return new Color(0, 0, 205, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumOrchid" with the value (186, 85, 211, 255)
        /// </summary>
        public static Color MediumOrchid
        {
            get { return new Color(186, 85, 211, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumPurple" with the value (147, 112, 219, 255)
        /// </summary>
        public static Color MediumPurple
        {
            get { return new Color(147, 112, 219, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumSeaGreen" with the value (60, 179, 113, 255)
        /// </summary>
        public static Color MediumSeaGreen
        {
            get { return new Color(60, 179, 113, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumSlateBlue" with the value (123, 104, 238, 255)
        /// </summary>
        public static Color MediumSlateBlue
        {
            get { return new Color(123, 104, 238, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumSpringGreen" with the value (0, 250, 154, 255)
        /// </summary>
        public static Color MediumSpringGreen
        {
            get { return new Color(0, 250, 154, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumTurquoise" with the value (72, 209, 204, 255)
        /// </summary>
        public static Color MediumTurquoise
        {
            get { return new Color(72, 209, 204, 255); }
        }

        /// <summary>
        /// Return a Color object "MediumVioletRed" with the value (199, 21, 133, 255)
        /// </summary>
        public static Color MediumVioletRed
        {
            get { return new Color(199, 21, 133, 255); }
        }

        /// <summary>
        /// Return a Color object "MidnightBlue" with the value (25, 25, 112, 255)
        /// </summary>
        public static Color MidnightBlue
        {
            get { return new Color(25, 25, 112, 255); }
        }

        /// <summary>
        /// Return a Color object "MintCream" with the value (245, 255, 250, 255)
        /// </summary>
        public static Color MintCream
        {
            get { return new Color(245, 255, 250, 255); }
        }

        /// <summary>
        /// Return a Color object "MistyRose" with the value (255, 228, 225, 255)
        /// </summary>
        public static Color MistyRose
        {
            get { return new Color(255, 228, 225, 255); }
        }

        /// <summary>
        /// Return a Color object "Moccasin" with the value (255, 228, 181, 255)
        /// </summary>
        public static Color Moccasin
        {
            get { return new Color(255, 228, 181, 255); }
        }

        /// <summary>
        /// Return a Color object "NavajoWhite" with the value (255, 222, 173, 255)
        /// </summary>
        public static Color NavajoWhite
        {
            get { return new Color(255, 222, 173, 255); }
        }

        /// <summary>
        /// Return a Color object "Navy" with the value (0, 0, 128, 255)
        /// </summary>
        public static Color Navy
        {
            get { return new Color(0, 0, 128, 255); }
        }

        /// <summary>
        /// Return a Color object "OldLace" with the value (253, 245, 230, 255)
        /// </summary>
        public static Color OldLace
        {
            get { return new Color(253, 245, 230, 255); }
        }

        /// <summary>
        /// Return a Color object "Olive" with the value (128, 128, 0, 255)
        /// </summary>
        public static Color Olive
        {
            get { return new Color(128, 128, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "OliveDrab" with the value (107, 142, 35, 255)
        /// </summary>
        public static Color OliveDrab
        {
            get { return new Color(107, 142, 35, 255); }
        }

        /// <summary>
        /// Return a Color object "Orange" with the value (255, 165, 0, 255)
        /// </summary>
        public static Color Orange
        {
            get { return new Color(255, 165, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "OrangeRed" with the value (255, 69, 0, 255)
        /// </summary>
        public static Color OrangeRed
        {
            get { return new Color(255, 69, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "Orchid" with the value (218, 112, 214, 255)
        /// </summary>
        public static Color Orchid
        {
            get { return new Color(218, 112, 214, 255); }
        }

        /// <summary>
        /// Return a Color object "PaleGoldenrod" with the value (238, 232, 170, 255)
        /// </summary>
        public static Color PaleGoldenrod
        {
            get { return new Color(238, 232, 170, 255); }
        }

        /// <summary>
        /// Return a Color object "PaleGreen" with the value (152, 251, 152, 255)
        /// </summary>
        public static Color PaleGreen
        {
            get { return new Color(152, 251, 152, 255); }
        }

        /// <summary>
        /// Return a Color object "PaleTurquoise" with the value (175, 238, 238, 255)
        /// </summary>
        public static Color PaleTurquoise
        {
            get { return new Color(175, 238, 238, 255); }
        }

        /// <summary>
        /// Return a Color object "PaleVioletRed" with the value (219, 112, 147, 255)
        /// </summary>
        public static Color PaleVioletRed
        {
            get { return new Color(219, 112, 147, 255); }
        }

        /// <summary>
        /// Return a Color object "PapayaWhip" with the value (255, 239, 213, 255)
        /// </summary>
        public static Color PapayaWhip
        {
            get { return new Color(255, 239, 213, 255); }
        }

        /// <summary>
        /// Return a Color object "PeachPuff" with the value (255, 218, 185, 255)
        /// </summary>
        public static Color PeachPuff
        {
            get { return new Color(255, 218, 185, 255); }
        }

        /// <summary>
        /// Return a Color object "Peru" with the value (205, 133, 63, 255)
        /// </summary>
        public static Color Peru
        {
            get { return new Color(205, 133, 63, 255); }
        }

        /// <summary>
        /// Return a Color object "Pink" with the value (255, 192, 203, 255)
        /// </summary>
        public static Color Pink
        {
            get { return new Color(255, 192, 203, 255); }
        }

        /// <summary>
        /// Return a Color object "Plum" with the value (221, 160, 221, 255)
        /// </summary>
        public static Color Plum
        {
            get { return new Color(221, 160, 221, 255); }
        }

        /// <summary>
        /// Return a Color object "PowderBlue" with the value (176, 224, 230, 255)
        /// </summary>
        public static Color PowderBlue
        {
            get { return new Color(176, 224, 230, 255); }
        }

        /// <summary>
        /// Return a Color object "Purple" with the value (128, 0, 128, 255)
        /// </summary>
        public static Color Purple
        {
            get { return new Color(128, 0, 128, 255); }
        }

        /// <summary>
        /// Return a Color object "Red" with the value (255, 0, 0, 255)
        /// </summary>
        public static Color Red
        {
            get { return new Color(255, 0, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "RosyBrown" with the value (188, 143, 143, 255)
        /// </summary>
        public static Color RosyBrown
        {
            get { return new Color(188, 143, 143, 255); }
        }

        /// <summary>
        /// Return a Color object "RoyalBlue" with the value (65, 105, 225, 255)
        /// </summary>
        public static Color RoyalBlue
        {
            get { return new Color(65, 105, 225, 255); }
        }

        /// <summary>
        /// Return a Color object "SaddleBrown" with the value (139, 69, 19, 255)
        /// </summary>
        public static Color SaddleBrown
        {
            get { return new Color(139, 69, 19, 255); }
        }

        /// <summary>
        /// Return a Color object "Salmon" with the value (250, 128, 114, 255)
        /// </summary>
        public static Color Salmon
        {
            get { return new Color(250, 128, 114, 255); }
        }

        /// <summary>
        /// Return a Color object "SandyBrown" with the value (244, 164, 96, 255)
        /// </summary>
        public static Color SandyBrown
        {
            get { return new Color(244, 164, 96, 255); }
        }

        /// <summary>
        /// Return a Color object "SeaGreen" with the value (46, 139, 87, 255)
        /// </summary>
        public static Color SeaGreen
        {
            get { return new Color(46, 139, 87, 255); }
        }

        /// <summary>
        /// Return a Color object "SeaShell" with the value (255, 245, 238, 255)
        /// </summary>
        public static Color SeaShell
        {
            get { return new Color(255, 245, 238, 255); }
        }

        /// <summary>
        /// Return a Color object "Sienna" with the value (160, 82, 45, 255)
        /// </summary>
        public static Color Sienna
        {
            get { return new Color(160, 82, 45, 255); }
        }

        /// <summary>
        /// Return a Color object "Silver" with the value (192, 192, 192, 255)
        /// </summary>
        public static Color Silver
        {
            get { return new Color(192, 192, 192, 255); }
        }

        /// <summary>
        /// Return a Color object "SkyBlue" with the value (135, 206, 235, 255)
        /// </summary>
        public static Color SkyBlue
        {
            get { return new Color(135, 206, 235, 255); }
        }

        /// <summary>
        /// Return a Color object "SlateBlue" with the value (106, 90, 205, 255)
        /// </summary>
        public static Color SlateBlue
        {
            get { return new Color(106, 90, 205, 255); }
        }

        /// <summary>
        /// Return a Color object "SlateGray" with the value (112, 128, 144, 255)
        /// </summary>
        public static Color SlateGray
        {
            get { return new Color(112, 128, 144, 255); }
        }

        /// <summary>
        /// Return a Color object "Snow" with the value (255, 250, 250, 255)
        /// </summary>
        public static Color Snow
        {
            get { return new Color(255, 250, 250, 255); }
        }

        /// <summary>
        /// Return a Color object "SpringGreen" with the value (0, 255, 127, 255)
        /// </summary>
        public static Color SpringGreen
        {
            get { return new Color(0, 255, 127, 255); }
        }

        /// <summary>
        /// Return a Color object "SteelBlue" with the value (70, 130, 180, 255)
        /// </summary>
        public static Color SteelBlue
        {
            get { return new Color(70, 130, 180, 255); }
        }

        /// <summary>
        /// Return a Color object "Tan" with the value (210, 180, 140, 255)
        /// </summary>
        public static Color Tan
        {
            get { return new Color(210, 180, 140, 255); }
        }

        /// <summary>
        /// Return a Color object "Teal" with the value (0, 128, 128, 255)
        /// </summary>
        public static Color Teal
        {
            get { return new Color(0, 128, 128, 255); }
        }

        /// <summary>
        /// Return a Color object "Thistle" with the value (216, 191, 216, 255)
        /// </summary>
        public static Color Thistle
        {
            get { return new Color(216, 191, 216, 255); }
        }

        /// <summary>
        /// Return a Color object "Tomato" with the value (255, 99, 71, 255)
        /// </summary>
        public static Color Tomato
        {
            get { return new Color(255, 99, 71, 255); }
        }

        /// <summary>
        /// Return a Color object "Transparent" with the value (0, 0, 0, 0)
        /// </summary>
        public static Color Transparent
        {
            get { return new Color(0, 0, 0, 0); }
        }

        /// <summary>
        /// Return a Color object "Turquoise" with the value (64, 224, 208, 255)
        /// </summary>
        public static Color Turquoise
        {
            get { return new Color(64, 224, 208, 255); }
        }

        /// <summary>
        /// Return a Color object "Violet" with the value (238, 130, 238, 255)
        /// </summary>
        public static Color Violet
        {
            get { return new Color(238, 130, 238, 255); }
        }

        /// <summary>
        /// Return a Color object "Wheat" with the value (245, 222, 179, 255)
        /// </summary>
        public static Color Wheat
        {
            get { return new Color(245, 222, 179, 255); }
        }

        /// <summary>
        /// Return a Color object "White" with the value (255, 255, 255, 255)
        /// </summary>
        public static Color White
        {
            get { return new Color(255, 255, 255, 255); }
        }

        /// <summary>
        /// Return a Color object "WhiteSmoke" with the value (245, 245, 245, 255)
        /// </summary>
        public static Color WhiteSmoke
        {
            get { return new Color(245, 245, 245, 255); }
        }

        /// <summary>
        /// Return a Color object "Yellow" with the value (255, 255, 0, 255)
        /// </summary>
        public static Color Yellow
        {
            get { return new Color(255, 255, 0, 255); }
        }

        /// <summary>
        /// Return a Color object "YellowGreen" with the value (154, 205, 50, 255)
        /// </summary>
        public static Color YellowGreen
        {
            get { return new Color(154, 205, 50, 255); }
        }
    }
}
