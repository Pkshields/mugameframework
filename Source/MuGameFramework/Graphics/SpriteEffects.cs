﻿using System;

namespace MuGameFramework.Graphics
{
    /// <summary>
    /// Different effects supported by sprites
    /// </summary>
    [Flags]
    public enum SpriteEffects
    {
        /// <summary>
        /// Flip the sprite Horizontally around it's origin
        /// </summary>
        FlipHorizontally = 0x1,

        /// <summary>
        /// Flip the sprite Vertically around it's origin
        /// </summary>
        FlipVertically = 0x2,

        /// <summary>
        /// Apply no effects
        /// </summary>
        None = 0x4
    }
}
