﻿using System;
using SFMLCircleShape = SFML.Graphics.CircleShape;

namespace MuGameFramework.Graphics.Shapes
{
    /// <summary>
    /// Create a visual Circle Shape to show on screen
    /// </summary>
    public class CircleShape : Shape
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Circle shape instance
        /// </summary>
        private SFMLCircleShape shape;

        /// <summary>
        /// Set the radius of the circle
        /// </summary>
        public float Radius
        {
            get { return shape.Radius; }
            set
            {
                shape.Radius = value;
                CalculateRectangle();
            }
        }

                                                #endregion PROPERTIES

                                                #region METHODS

        /// <summary>
        /// Initialize an instance of a drawable Circle Shape
        /// </summary>
        /// <param name="radius"></param>
        public CircleShape(float radius)
        {
            shape = new SFMLCircleShape(radius);
            Initialize(shape);
        }

                                                #endregion METHODS
    }
}
