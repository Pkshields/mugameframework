﻿using System;
using MuGameFramework.Components;
using MuGameFramework.Utilities;
using SFMLShape = SFML.Graphics.Shape;
using SFMLColor = SFML.Graphics.Color;
using SFMLVector2f = SFML.Window.Vector2f;
using SFMLFloatRect = SFML.Graphics.FloatRect;

namespace MuGameFramework.Graphics.Shapes
{
    /// <summary>
    /// Abstract Shape representing the generic methods all shapes have
    /// </summary>
    public abstract class Shape : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Shape instance
        /// </summary>
        internal SFMLShape genShape;

        /// <summary>
        /// Position of the shape on screen
        /// </summary>
        public Vector2 Position
        {
            get { return new Vector2(genShape.Position.X, genShape.Position.Y); }
            set { genShape.Position = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Origin to draw the shape from
        /// </summary>
        public Vector2 Origin
        {
            get { return new Vector2(genShape.Origin.X, genShape.Origin.Y); }
            set { genShape.Origin = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Color of the body of the shape
        /// </summary>
        public Color FillColor
        {
            get { return new Color(genShape.FillColor.R, genShape.FillColor.G, genShape.FillColor.B, genShape.FillColor.A); }
            set { genShape.FillColor = value.AsSFMLColor; }
        }

        /// <summary>
        /// Color of the outline of the shape
        /// </summary>
        public Color OutlineColor
        {
            get { return new Color(genShape.OutlineColor.R, genShape.OutlineColor.G, genShape.OutlineColor.B, genShape.OutlineColor.A); }
            set { genShape.OutlineColor = value.AsSFMLColor; }
        }

        /// <summary>
        /// Thickness of the outline of the shape
        /// </summary>
        public float OutlineThickness
        {
            get { return genShape.OutlineThickness; }
            set { genShape.OutlineThickness = value; }
        }

        /// <summary>
        /// Rotation of the shape in degrees
        /// </summary>
        public float Rotation
        {
            get { return genShape.Rotation; }
            set { genShape.Rotation = value; }
        }

        /// <summary>
        /// Scale of the sprite
        /// </summary>
        public Vector2 Scale
        {
            get { return new Vector2(genShape.Scale.X, genShape.Scale.Y); }
            set { genShape.Scale = value.AsSFMLVector2f; }
        }

        /// <summary>
        /// Texture to apply to the shape
        /// </summary>
        public Texture2D Texture
        {
            get { return new Texture2D(genShape.Texture); }
            set { genShape.Texture = value.texture; }
        }

        /// <summary>
        /// Rectangle of the shape to draw on screen
        /// </summary>
        public Rectangle SourceRectangle
        {
            get { return new Rectangle(genShape.TextureRect.Left, genShape.TextureRect.Top, genShape.TextureRect.Width, genShape.TextureRect.Height); }
            set { genShape.TextureRect = value.AsSFMLIntRect; }
        }

        /// <summary>
        /// Get the number of points contained in the shape
        /// </summary>
        public int PointCount
        {
            get { return (int)genShape.GetPointCount(); }
        }

        /// <summary>
        /// Effects to apply to the shape
        /// </summary>
        public SpriteEffects Effects = SpriteEffects.None;

        /// <summary>
        /// Full rectangle containing the shape
        /// </summary>
        public Rectangle FullRectangle
        {
            get;
            private set;
        }

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        public bool Disposed
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Constructor for the general section of the shape
        /// </summary>
        public Shape()
        { }

        /// <summary>
        /// Initialize the general section of the shape
        /// </summary>
        /// <param name="shape">Shape that has been created</param>
        protected void Initialize(SFMLShape shape)
        {
            //Store the shape then store the size of the rectangle containing the shape
            genShape = shape;
            CalculateRectangle();
            
        }

        /// <summary>
        /// Calculate the size of the containing bounding rectangle
        /// </summary>
        protected void CalculateRectangle()
        {
            SFMLFloatRect fullRect = genShape.GetLocalBounds();
            FullRectangle = new Rectangle(0, 0, (int)fullRect.Width, (int)fullRect.Height);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the simple positioned rectangle for this shape. 
        /// Neither Scale, Rotation or SpriteEffects are applied
        /// </summary>
        /// <returns>Simple positioned rectangle for this shape</returns>
        public Rectangle GetRectangle()
        {
            return new Rectangle((int)(Position.X - Origin.X),
                                 (int)(Position.X - Origin.X),
                                 (int)(FullRectangle.Width),
                                 (int)(FullRectangle.Height));
        }

        /// <summary>
        /// Get the simple positioned rectangle for this shape. 
        /// Scale is applied, but Rotation and SpriteEffects are not
        /// </summary>
        /// <returns>Simple positioned rectangle w. Scale</returns>
        public Rectangle GetScaledRectangle()
        {
            return new Rectangle((int)(Position.X - Origin.X),
                                 (int)(Position.Y - Origin.Y),
                                 (int)(FullRectangle.Width * Scale.X),
                                 (int)(FullRectangle.Height * Scale.Y));
        }

        /// <summary>
        /// Get the fully modified positioned rectangle for this shape. 
        /// Scale, Rotation and SpriteEffects are applied
        /// </summary>
        /// <returns>Fully modified positioned rectangle</returns>
        public Rectangle GetFullRectangle()
        {
            //Firstly, get the scaled rectangle
            Rectangle fullRect = GetScaledRectangle();

            //If the shape has been rotated, modify it accordingly
            if (genShape.Rotation != 0f)
            {
                //Convert the rotation to radians 
                float rotation = MathHelper.DegreesToRadians(genShape.Rotation);

                //Set up the 4 points of the current rectangle in Point structs for rotating
                Vector2[] fourPoints = { new Vector2(fullRect.X, fullRect.Y),                                       //Top Left
                                         new Vector2(fullRect.X, fullRect.Y + fullRect.Height),                     //Top Right
                                         new Vector2(fullRect.X + fullRect.Width, fullRect.Y),                      //Bottom Left
                                         new Vector2(fullRect.X + fullRect.Width, fullRect.Y + fullRect.Height) };  //Bottom Right

                //Rotate the four points around the origin point by the rotation angle
                for (int i = 0; i < fourPoints.Length; i++)
                    fourPoints[i] = MathHelper.RotatePoint(fourPoints[i], Position, rotation);

                //Set some (dummy) initial values to make the following loops work
                fullRect.X = (int)fourPoints[0].X;
                fullRect.Y = (int)fourPoints[0].Y;
                fullRect.Width = 0;
                fullRect.Height = 0;

                //Firstly, loop through the points, finding the top left coordinates
                for (int i = 1; i < fourPoints.Length; i++)
                {
                    if (fourPoints[i].X < fullRect.X) fullRect.X = (int)fourPoints[i].X;
                    if (fourPoints[i].Y < fullRect.Y) fullRect.Y = (int)fourPoints[i].Y;
                }

                //Then, once that is calculated, calculate the width and height
                for (int i = 0; i < fourPoints.Length; i++)
                {
                    if (fourPoints[i].X - fullRect.X > fullRect.Width) fullRect.Width = (int)fourPoints[i].X - fullRect.X;
                    if (fourPoints[i].Y - fullRect.Y > fullRect.Height) fullRect.Height = (int)fourPoints[i].Y - fullRect.Y;
                }

                //Calculation done! Values whould be in the FullRect
            }

            //If the shape has effects via SpriteEffects applied to it, modify accordingly
            if (Effects != SpriteEffects.None)
            {
                //If the shape is flipped horizontally, then move the X coordinate up accordingly
                // depending on the height of the shape and the position of the origin 
                if ((Effects & SpriteEffects.FlipHorizontally) == SpriteEffects.FlipHorizontally)
                    fullRect.X -= (int)(Math.Abs((fullRect.Width / 2f) - genShape.Origin.X) + (fullRect.Width / 2f));

                //Same to the vertical flip
                if ((Effects & SpriteEffects.FlipVertically) == SpriteEffects.FlipVertically)
                    fullRect.Y -= (int)(Math.Abs((fullRect.Height / 2f) - genShape.Origin.Y) + (fullRect.Height / 2f));
            }

            //Return the true modified rectangle
            return fullRect;
        }

        /// <summary>
        /// Get the position of a point in the shape
        /// </summary>
        /// <param name="index">Index of the point to get</param>
        /// <returns>Position of a point</returns>
        public Vector2 GetPoint(uint index)
        {
            SFMLVector2f point = genShape.GetPoint(index);
            return new Vector2(point.X, point.Y);
        }

                                                #endregion METHODS

                                                #region INTERFACE METHODS

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Unmanaged or large components
            if (genShape != null)
            {
                genShape.Dispose();
                genShape = null;
            }

            //Tell everyone else this is disposed
            this.Disposed = true;
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~Shape()
        {
            Dispose(false);
        }

        /// <summary>
        /// Is this Shape disposed?
        /// </summary>
        /// <returns>Is this Shape disposed?</returns>
        public bool IsDisposed()
        {
            return Disposed;
        }

                                                #endregion INTERFACE METHODS
    }
}
