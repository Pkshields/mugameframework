﻿using System;
using MuGameFramework.Components;
using SFMLRectangleShape = SFML.Graphics.RectangleShape;
using SFMLVector2F = SFML.Window.Vector2f;

namespace MuGameFramework.Graphics.Shapes
{
    /// <summary>
    /// Create a visual Rectangle Shape to show on screen
    /// </summary>
    public class RectangleShape : Shape
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Rectangle shape instance
        /// </summary>
        internal SFMLRectangleShape shape;

        /// <summary>
        /// Size of the rectangle
        /// </summary>
        public Size Size
        {
            get { return new Size((int)shape.Size.X, (int)shape.Size.Y); }
            set
            {
                shape.Size = new SFMLVector2F(value.Width, value.Height);
                CalculateRectangle();
            }
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of a drawable Rectangle Shape
        /// </summary>
        /// <param name="size">Width and height of the Rectangle</param>
        public RectangleShape(Size size)
        {
            shape = new SFMLRectangleShape(new SFMLVector2F(size.Width, size.Height));
            Initialize(shape);
        }

        /// <summary>
        /// Initialize an instance of a drawable Rectangle Shape
        /// </summary>
        /// <param name="width">Width of the Rectangle</param>
        /// <param name="height">Height of the Rectangle</param>
        public RectangleShape(int width, int height)
        {
            shape = new SFMLRectangleShape(new SFMLVector2F(width, height));
            Initialize(shape);
        }

                                                #endregion CONSTRUCTORS
    }
}
