﻿using System;
using MuGameFramework.Components;
using SFMLConvexShape = SFML.Graphics.ConvexShape;

namespace MuGameFramework.Graphics.Shapes
{
    /// <summary>
    /// Create a visual Shape of many sides to show on screen
    /// </summary>
    public class PolygonShape : Shape
    {
                                                #region PROPERTIES

        /// <summary>
        /// SFML Convex shape instance
        /// </summary>
        private SFMLConvexShape shape;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of a drawable Polygon Shape
        /// </summary>
        /// <param name="pointArray">Array of relative points that make up the PolygonShape</param>
        public PolygonShape(Vector2[] pointArray)
        {
            //Initialize the SFML shape
            shape = new SFMLConvexShape();

            //Initialise the points in thus shape
            Reinitialize(pointArray);
        }

        /// <summary>
        /// ReInitialize this shape as another shape
        /// </summary>
        /// <param name="pointArray">Array of relative points that make up the PolygonShape</param>
        public void Reinitialize(Vector2[] pointArray)
        {
            //Tell the object the amount of points we have in our shape
            uint arrayLength = (uint)pointArray.Length;
            shape.SetPointCount(arrayLength);

            //Set the relative position for each of the points we just told the object we have
            for (uint i = 0; i < arrayLength; i++)
                shape.SetPoint(i, pointArray[i].AsSFMLVector2f);

            //Run the Shape initialization
            Initialize(shape);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Change the relative position of a point in the shape
        /// </summary>
        /// <param name="index">Index of point to change</param>
        /// <param name="point">New position of thus point</param>
        public void ChangePoint(uint index, Vector2 point)
        {
            shape.SetPoint(index, point.AsSFMLVector2f);
        }

                                                #endregion METHODS
    }
}
