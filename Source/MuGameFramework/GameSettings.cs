using System;
using MuGameFramework.Components;
using MuGameFramework.InputDevice;

namespace MuGameFramework
{
    /// <summary>
    /// Used to provide the settings to the GameEngine
    /// </summary>
    public class GameSettings
    {
        /// <summary>
        /// Is the game running at a Variable or Fixed timestep (framerate)
        /// </summary>
        public bool IsFixedTimestep = true;

        /// <summary>
        /// Framerate for the game to display at. 
        /// </summary>
        public int FixedFramerate = 60;

        /// <summary>
        /// Title for the window
        /// </summary>
        public string WindowName = "Video Game";

        /// <summary>
        /// Size and position of the window
        /// </summary>
        public Rectangle WindowSizeLoc = new Rectangle(0, 0, 1024, 576);

        /// <summary>
        /// Is the window generated to be fullscreen?
        /// </summary>
        public bool IsFullscreen = false;

        /// <summary>
        /// Initial directory holding all the games content
        /// </summary>
        public string ContentDir = "Content";

        /// <summary>
        /// Initialize an instance of GameSettings with the default settings
        /// </summary>
        public GameSettings()
        { }
    }
}
