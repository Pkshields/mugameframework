﻿using System;
using MuGameFramework.Components;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Object holding a certain state of the Mouse
    /// </summary>
    public struct MouseState
    {
                                                #region PROPERTIES

        /// <summary>
        /// Store the current state of the MouseButtons
        /// </summary>
        private MouseButtons mouseButtons;

        /// <summary>
        /// Store the current state of the Mouse Position (in rectangle form)
        /// </summary>
        private Rectangle mousePos;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of MouseState
        /// </summary>
        /// <param name="mouseButtons">Mouse buttons currently being pressed</param>
        /// <param name="position">Current position of the mouse</param>
        internal MouseState(MouseButtons mouseButtons, Point position)
        {
            this.mouseButtons = mouseButtons;
            this.mousePos = new Rectangle(position.X, position.Y, 1, 1);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current position of the mouse
        /// </summary>
        /// <returns>Current position of the mouse</returns>
        public Vector2 GetMousePosition()
        {
            //Return the current position of the mouse
            return new Vector2(mousePos.X, mousePos.Y);
        }

        /// <summary>
        /// Check if the mouse is intersecting a rectangle at a certain location
        /// </summary>
        /// <param name="checkRect">Rectangle we are checking intersection with</param>
        /// <returns>Result from intersection check</returns>
        public bool IsMouseIntersect(Rectangle checkRect)
        {
            //Return the result of the collision test between argument and mouse
            return mousePos.Intersects(checkRect);
        }

        /// <summary>
        /// Check if the left mouse button is down
        /// </summary>
        /// <returns>Return if the left mouse button is down</returns>
        public bool IsMouseButtonDown(MouseButtons button)
        {
            //Take the current mouse buttons, AND it with the button we are looking for to remove the other buttons,
            //and see if the buttons we are looking for is included
            //If it is, then the button is down, else it is up
            return ((mouseButtons & button) == button);
        }

        /// <summary>
        /// Check if the left mouse button is up
        /// </summary>
        /// <returns>Return if the left mouse button is up</returns>
        public bool IsMouseButtonUp(MouseButtons button)
        {
            //See IsMouseButtonDown for explanation of code
            return ((mouseButtons & button) != button);
        }

                                                #endregion METHODS
    }
}
