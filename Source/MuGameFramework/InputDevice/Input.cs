﻿using System;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Static class controlling the 3 input types
    /// </summary>
    internal static class Input
    {
        /// <summary>
        /// Window initialized for use with Input
        /// </summary>
        internal static Window InputWindow;

        /// <summary>
        /// Initialize all the Input classes
        /// </summary>
        /// <param name="window">Window run by this Framework</param>
        public static void Initialize(Window window)
        {
            //Store the Input window
            InputWindow = window;

            //Initialize all the input devices
            Keyboard.Initialize(window);
            Mouse.Initialize(window);
            Joystick.Initialize(window);
        }

        /// <summary>
        /// Update all the Input classes
        /// </summary>
        public static void Update()
        {
            //Currently just update the Joystick
            Joystick.Update();
        }
    }
}
