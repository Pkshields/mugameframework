﻿using System;
using SFMLJoystick = SFML.Window.Joystick;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Object holding a certain state of the Joystick
    /// </summary>
    public struct JoystickState
    {
                                                #region PROPERTIES

        /// <summary>
        /// ID of the joystick
        /// </summary>
        private uint joystickID;

        /// <summary>
        /// Pressed buttons of this joystick in Int32 form
        /// </summary>
        private Int32 joystickButtons;

        /// <summary>
        /// Values of all the axis on the joystick
        /// </summary>
        private float[] joystickAxis;

        /// <summary>
        /// Is the Joystick connected
        /// </summary>
        public bool IsConnected
        {
            get;
            private set;
        }

        /// <summary>
        /// Value limit that - if below - movement will not be recognised by the JoystickState
        /// </summary>
        public float DeadZone
        {
            get;
            private set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of JoystickState
        /// </summary>
        /// <param name="joystickID">ID of the joystick to poll</param>
        /// <param name="pressedButtons">Current state of the buttons for this joystick</param>
        /// <param name="deadZone">Value that which below, the axis' will return the value 0</param>
        /// <param name="isEmpty">Should this JoystickState just represent an untouched Joystick</param>
        internal JoystickState(int joystickID, Int32 pressedButtons, float deadZone, bool isEmpty = false)
            : this()
        {
            //Store the deadzone value
            this.DeadZone = deadZone;

            //Check if the joystick connected
            this.IsConnected = SFMLJoystick.IsConnected((uint)joystickID);

            //If the Joystick isn't supposed to be empty, fill it with proper info
            //Also check if the joystick is connected. If it isn't, no point in trying to get any data from it
            if (isEmpty || IsConnected)
            {
                //Store the passed in values
                this.joystickID = (uint)joystickID;
                this.joystickButtons = pressedButtons;

                //Get all the axis position for this joystick
                //All values divided by 100 to keep them within a constant range of 0..1
                this.joystickAxis = new float[] { SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.X) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.Y) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.Z) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.R) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.U) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.V) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.PovX) / 100, 
                                                  SFMLJoystick.GetAxisPosition(this.joystickID, SFMLJoystick.Axis.PovY) / 100};
            }
            //Else, fill it with data representing empty Joystick
            else
            {
                this.joystickID = (uint)joystickID;
                this.joystickButtons = 0;
                this.joystickAxis = new float[] { 0, 0, 0, 0, 0, 0, 0, 0};
            }
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current position of a joystick axis for this Joystick
        /// </summary>
        /// <param name="axis">Axis to check</param>
        /// <returns>Position of the Axis</returns>
        public float GetAxisPosition(JoystickAxis axis)
        {
            //Apply the deadzone value to it
            return (Math.Abs(joystickAxis[(int)axis]) > DeadZone ? joystickAxis[(int)axis] : 0);
        }

        /// <summary>
        /// Check if a button on the joystick is pressed
        /// </summary>
        /// <param name="button">Button to check</param>
        /// <returns>Result of check</returns>
        public bool IsButtonDown(int button)
        {
            //Check that the inserted button is in range
            if (IsButtonInRange(button))
                //Use Bitwise AND to get the button press bit from the Int32
                return ((joystickButtons & 1 << button) != 0);
            else
                throw new JoystickOutOfRangeException("Button is out of range");
        }

        /// <summary>
        /// Check if a button on the joystick is unpressed
        /// </summary>
        /// <param name="button">Button to check</param>
        /// <returns>Result of check</returns>
        public bool IsButtonUp(int button)
        {
            //Check that the inserted button is in range
            if (IsButtonInRange(button))
                return !((joystickButtons & 1 << button) != 0);
            else
                throw new JoystickOutOfRangeException("Button is out of range");
        }

        /// <summary>
        /// Check that the inputted JoystickID is in rnge of possible values
        /// </summary>
        /// <param name="button">Button to check</param>
        /// <returns>Result of check</returns>
        private bool IsButtonInRange(int button)
        {
            return (button >= 0 && button < Joystick.ButtonCount);
        }

                                                #endregion METHODS

                                                #region EQUALITY METHODS

        /// <summary>
        /// Compare two JoystickStates for equality
        /// </summary>
        /// <param name="a">JoystickStates A to check</param>
        /// <param name="b">JoystickStates B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(JoystickState a, JoystickState b)
        {
            //Check that either JoystickState is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return ((a.joystickButtons == b.joystickButtons) && (a.joystickAxis == b.joystickAxis));
        }

        /// <summary>
        /// Compare two JoystickState for inequality
        /// </summary>
        /// <param name="a">JoystickState A to check</param>
        /// <param name="b">JoystickState B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(JoystickState a, JoystickState b)
        {
            //Check that either KeyboardState is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return ((a.joystickButtons != b.joystickButtons) && (a.joystickAxis != b.joystickAxis));
        }

        /// <summary>
        /// Compare a JoystickState with this for equality
        /// </summary>
        /// <param name="obj">JoystickState to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is JoystickState)
                return (this == (JoystickState)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this objetc
                hash = hash * 23 + joystickButtons.GetHashCode();
                hash = hash * 23 + joystickAxis.GetHashCode();

                return hash;
            }
        }

                                                #endregion EQUALITY METHODS
    }
}
