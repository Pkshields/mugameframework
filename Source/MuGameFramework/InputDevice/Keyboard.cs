﻿using System;
using System.Linq;
using SFML.Window;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Static class controlling all Keyboard operations
    /// </summary>
    public static class Keyboard
    {
                                                #region PROPERTIES

        /// <summary>
        /// Array containing the current state of the keyboard
        /// </summary>
        private static bool[] keyboardArray;

        /// <summary>
        /// Array containing a state of the keyboard where all keys are released
        /// </summary>
        private static bool[] emptyKeyboardArray;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the static Keyabord class
        /// </summary>
        /// <param name="window">Window run by this Framework</param>
        internal static void Initialize(Window window)
        {
            //Assign methods from this class to the keyboard events in the window
            window.RenderWindow.KeyPressed += new EventHandler<KeyEventArgs>(RenderWindow_KeyPressed);
            window.RenderWindow.KeyReleased += new EventHandler<KeyEventArgs>(RenderWindow_KeyReleased);

            //Initialize the keyboard array (to all keyup)
            keyboardArray = Enumerable.Repeat<bool>(false, (int)Key.KeyCount).ToArray();

            //Initialize the empty array withthe same, except this will be always keyup
            emptyKeyboardArray = Enumerable.Repeat<bool>(false, (int)Key.KeyCount).ToArray();
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current state of the Keyboard
        /// </summary>
        /// <returns>Current state of the keyboard</returns>
        public static KeyboardState GetState()
        {
            //If the window has foxus then send the current state, else send a state with all the keys up
            if (Input.InputWindow.HasFocus)
                return new KeyboardState(keyboardArray);
            else
                return new KeyboardState(emptyKeyboardArray);
        }

        /// <summary>
        /// Event method called when a keyboard button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void RenderWindow_KeyPressed(object sender, KeyEventArgs e)
        {
            keyboardArray[(int)e.Code] = true;
        }

        /// <summary>
        /// Event method called when a keyboard button is released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void RenderWindow_KeyReleased(object sender, KeyEventArgs e)
        {
            keyboardArray[(int)e.Code] = false;
        }

                                                #endregion METHODS
    }
}

