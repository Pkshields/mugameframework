﻿using System;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Implement the MouseButtons from System.Windows.Forms into MuGameEngine.InputDevice for easy access for end user
    /// </summary>
    [Flags]
    public enum MouseButtons
    {
        Left = System.Windows.Forms.MouseButtons.Left,
        Middle = System.Windows.Forms.MouseButtons.Middle,
        None = System.Windows.Forms.MouseButtons.None,
        Right = System.Windows.Forms.MouseButtons.Right,
        XButton1 = System.Windows.Forms.MouseButtons.XButton1,
        XButton2 = System.Windows.Forms.MouseButtons.XButton2
    }
}
