﻿using System;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Enumeration representing the axis supported by Joystick
    /// </summary>
    public enum JoystickAxis
    {
        X,
        Y,
        Z,
        R,
        U,
        V,
        PovX,
        PovY
    };
}
