﻿using System;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Object holding a certain state of the Keyboard
    /// </summary>
    public struct KeyboardState
    {
                                                #region PROPERTIES

        /// <summary>
        /// Current array of keyboard key presses
        /// </summary>
        private bool[] keyboardArray;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize an instance of KeyboardState
        /// </summary>
        /// <param name="currentState">Current array of keyboard key presses</param>
        internal KeyboardState(bool[] currentState)
        {
            //Store this array of keyboard presses by copying the passed in array
            keyboardArray = new bool[(int)Key.KeyCount];
            Buffer.BlockCopy(currentState, 0, keyboardArray, 0, keyboardArray.Length);
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Check if a Keyboard key is down
        /// </summary>
        /// <returns>Return if a Keyboard key is down</returns>
        public bool IsKeyDown(Key key)
        {
            return keyboardArray[(int)key];
        }

        /// <summary>
        /// Check if a Keyboard key is up
        /// </summary>
        /// <returns>Return if a Keyboard key is up</returns>
        public bool IsKeyUp(Key key)
        {
            return !keyboardArray[(int)key];
        }

                                                #endregion METHODS

                                                #region EQUALITY METHODS

        /// <summary>
        /// Compare two KeyboardStates for equality
        /// </summary>
        /// <param name="a">KeyboardState A to check</param>
        /// <param name="b">KeyboardState B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(KeyboardState a, KeyboardState b)
        {
            //Check that either KeyboardState is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.keyboardArray == b.keyboardArray);
        }

        /// <summary>
        /// Compare two KeyboardStates for inequality
        /// </summary>
        /// <param name="a">KeyboardState A to check</param>
        /// <param name="b">KeyboardState B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(KeyboardState a, KeyboardState b)
        {
            //Check that either KeyboardState is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.keyboardArray != b.keyboardArray);
        }

        /// <summary>
        /// Compare a KeyboardState with this for equality
        /// </summary>
        /// <param name="obj">KeyboardState to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is KeyboardState)
                return (this == (KeyboardState)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this objetc
                hash = hash * 23 + keyboardArray.GetHashCode();

                return hash;
            }
        }

                                                #endregion EQUALITY METHODS
    }
}
