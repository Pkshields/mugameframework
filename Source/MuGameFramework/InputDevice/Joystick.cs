﻿using System;
using System.Linq;
using SFML.Window;
using SFMLJoystick = SFML.Window.Joystick;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Static class controlling all Joystick operations
    /// </summary>
    public static class Joystick
    {
                                                #region PROPERTIES

        /// <summary>
        /// Int32 containing the currently pressed buttons for all joysticks
        /// </summary>
        private static Int32[] joystickButtons;

        /// <summary>
        /// Int32 representing unpressed set of buttons
        /// </summary>
        private static Int32 emptyJoystickButtons;

        /// <summary>
        /// Value limit that - if below - movement will not be recognised by the JoystickState. 
        /// Default value: 0.15f
        /// </summary>
        public static float DeadZone
        {
            get;
            set;
        }

        /// <summary>
        /// Maximum amount of Joysticks that is supported
        /// </summary>
        public static readonly int MaxCount = (int)SFMLJoystick.Count;

        /// <summary>
        /// Total amount of buttons that are supported 
        /// </summary>
        internal static readonly int ButtonCount = (int)SFMLJoystick.ButtonCount;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the static Joystick class
        /// </summary>
        /// <param name="window">Window run by this Framework</param>
        internal static void Initialize(Window window)
        {
            //Set the default DeadZone value
            DeadZone = 0.15f;

            //Assign methods from this class to the keyboard events in the window
            window.RenderWindow.JoystickButtonPressed += new EventHandler<JoystickButtonEventArgs>(RenderWindow_JoystickButtonPressed);
            window.RenderWindow.JoystickButtonReleased += new EventHandler<JoystickButtonEventArgs>(RenderWindow_JoystickButtonReleased);

            //Initialize the button int (to all buttons unpressed)
            joystickButtons = Enumerable.Repeat<int>(0, (int)SFMLJoystick.Count).ToArray();

            //Initialize the empty button int to an empty number (0)
            emptyJoystickButtons = 0;
        }

        /// <summary>
        /// Update all the joysticks
        /// </summary>
        internal static void Update()
        {
            //Run the Update script
            SFMLJoystick.Update();
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current state of the Joystick
        /// </summary>
        /// <returns>Current state of the joystick</returns>
        public static JoystickState GetState(int joystickID)
        {
            //Ensure that the joystickID is in range
            if (IsWithinJoystickRange(joystickID))
            {
                //If the window has focus then send the current state, else send a state with all the keys up
                if (Input.InputWindow.HasFocus)
                    return new JoystickState(joystickID, joystickButtons[joystickID], DeadZone);
                else
                    return new JoystickState(joystickID, emptyJoystickButtons, DeadZone, true);
            }
            else
                throw new JoystickOutOfRangeException("Inputted Joystick ID is out of range");
        }

        /// <summary>
        /// Check if a gamepad is connected
        /// </summary>
        /// <param name="joystickID">Gamepad to check</param>
        /// <returns>If the gamepad is connected</returns>
        public static bool IsConnected(int joystickID)
        {
            //Ensure that the joystickID is in range
            if (IsWithinJoystickRange(joystickID))
                return SFMLJoystick.IsConnected((uint)joystickID);
            else
                throw new JoystickOutOfRangeException("Inputted Joystick ID is out of range");
        }

        /// <summary>
        /// Check that the inputted JoystickID is in rnge of possible values
        /// </summary>
        /// <param name="joystickID">ID to check</param>
        /// <returns>Result of check</returns>
        private static bool IsWithinJoystickRange(int joystickID)
        {
            return (joystickID >= 0 && joystickID < MaxCount);
        }

        /// <summary>
        /// Event method called when a joystick button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void RenderWindow_JoystickButtonPressed(object sender, JoystickButtonEventArgs e)
        {
            //Convert the pressed button into the corresponding bit in the joystick buttons
            //Then set the bit within the buttons int using Bitwise OR
            int buttonPressed = 1 << (int)e.Button;
            joystickButtons[e.JoystickId] |= (1 << (int)e.Button);
        }

        /// <summary>
        /// Event method called when a joystick button is released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void RenderWindow_JoystickButtonReleased(object sender, JoystickButtonEventArgs e)
        {
            //Convert the pressed button into the corresponding bit in the joystick buttons
            //Then take that and invert it using Bitwise NOT
            //Then, using Bitwise AND, unset the bit in the JoystickButtons int
            joystickButtons[e.JoystickId] &= ~(1 << (int)e.Button);
        }

                                                #endregion METHODS
    }
}

