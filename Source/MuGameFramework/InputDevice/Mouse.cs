﻿using System;
using System.Windows.Forms;
using MuGameFramework.Components;
using SFMLMouse = SFML.Window.Mouse;

namespace MuGameFramework.InputDevice
{
    /// <summary>
    /// Static class controlling all Mouse operations
    /// </summary>
    public static class Mouse
    {
                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the static Mouse class
        /// </summary>
        /// <param name="window">Window run by this Framework</param>
        internal static void Initialize(Window window)
        { }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Get the current state of the Mouse
        /// </summary>
        /// <returns>Current state of the Mouse</returns>
        public static MouseState GetState()
        {
            //If the window has focus then send the current state, else send a state with all the keys up
            if (Input.InputWindow.HasFocus)
            {
                Point mousePoint = new Point(SFMLMouse.GetPosition(Input.InputWindow.RenderWindow).X, 
                                             SFMLMouse.GetPosition(Input.InputWindow.RenderWindow).Y);
                return new MouseState((MouseButtons)Control.MouseButtons, mousePoint);
            }
            else
                return new MouseState(MouseButtons.None, Point.Zero);
        }

                                                #endregion METHODS
    }
}
