using System;
using MuGameFramework.Components;
using MuGameFramework.InputDevice;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Abstract class used to hold the code for a users game to interact within the game framework
    /// </summary>
    public abstract class GameCode
    {
                                                #region PROPERTIES

        /// <summary>
        /// Provide access to the engine's assets throughout the GameCode
        /// </summary>
        private GameFramework framework;

                                                #endregion PROPERTIES

                                                #region ACCESS PROPERTIES

        /// <summary>
        /// Object used to keep track of the gane time while the engine is running
        /// </summary>
        public GameTime GameTime
        {
            get { return framework.GameTime; }
        }

        /// <summary>
        /// Access the Window directly
        /// </summary>
        public Window Window
        {
            get { return framework.Window; }
        }

        /// <summary>
        /// Access the dimensions of the window
        /// </summary>
        public Size WindowDimensions
        {
            get { return framework.Window.WindowDimensions; }
        }

        /// <summary>
        /// Content Manager used to load and unload content from the game
        /// </summary>
        public ContentManager Content
        {
            get { return framework.Content; }
        }

        /// <summary>
        /// GameComponents List used to manage GameComponents
        /// </summary>
        public GameComponentList Components
        {
            get { return framework.Components; }
        }

        /// <summary>
        /// Global access to services via GameServices
        /// </summary>
        public GameServices Services
        {
            get { return framework.Services; }
        }

                                                #endregion ACCESS PROPERTIES

                                                #region CONSTRUCTOR

        /// <summary>
        /// Set the basic settings of the game
        /// </summary>
        /// <returns>Settings of the game</returns>
        public virtual GameSettings GameSettings()
        {
            //Send back the default settings if settings are not set by the user
            return new GameSettings();
        }

        /// <summary>
        /// Run some required code for this object before anything else gets executed
        /// </summary>
        /// <param name="framework">The running game framework</param>
        public void PreInitialize(GameFramework framework)
        {
            this.framework = framework;
        }

                                                #endregion CONSTRUCTOR

                                                #region VIRTUAL METHODS

        /// <summary>
        /// Initialize objects in the game
        /// </summary>
        public virtual void Initialize()
        { }

        /// <summary>
        /// Load content for thus objects in the game
        /// Runs before Initialize
        /// </summary>
        public virtual void LoadContent()
        { }

        /// <summary>
        /// Unload content loaded into the game
        /// </summary>
        public virtual void UnloadContent()
        { }

        /// <summary>
        /// Update the objects in the game every tick
        /// </summary>
        public virtual void Update()
        { }

        /// <summary>
        /// Draw the objects in the game every tick
        /// </summary>
        /// <param name="spriteBatch">SpriteBatch used to draw the objects</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        { }

                                                #endregion VIRTUAL METHODS
    }
}
