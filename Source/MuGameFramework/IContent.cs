﻿using System;

namespace MuGameFramework
{
    /// <summary>
    /// Interface ensuring all pieces of content have these methods
    /// </summary>
    public interface IContent : IDisposable
    {
        /// <summary>
        /// Load a file into this object (if file is not already loaded into it)
        /// </summary>
        /// <param name="fileToLoad">Location of file to load</param>
        void LoadContent(string fileToLoad);

        /// <summary>
        /// Is this Texture disposed?
        /// </summary>
        /// <returns>Is this Texture disposed?</returns>
        bool IsDisposed();
    }
}
