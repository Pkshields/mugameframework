﻿using System;
using System.Collections.Generic;

namespace MuGameFramework
{
    /// <summary>
    /// Object that can hold global services for access anywhere in the code
    /// </summary>
    public class GameServices
    {
                                                #region PROPERTIES

        /// <summary>
        /// Dictionary containing the stored services
        /// </summary>
        private Dictionary<Type, object> servicesDictionary;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Initialize the GameServices
        /// </summary>
        public GameServices()
        {
            servicesDictionary = new Dictionary<Type, object>();
        }

                                                #endregion CONSTRUCTORS
        
                                                #region METHODS

        /// <summary>
        /// Add a service to the manager
        /// </summary>
        /// <param name="service">Service to add</param>
        public void AddService(object service)
        {
            //Get the type of the service we are storing
            Type t = service.GetType();

            //As long as we don't have something of that type already stored, store it
            if (!servicesDictionary.ContainsKey(t))
                servicesDictionary.Add(t, service);

            //Else tell the user that something of that type is already stored
            else
                throw new ArgumentException("Game Services already contains a service for that type");
        }

        /// <summary>
        /// Get a stored service from the manager
        /// </summary>
        /// <typeparam name="T">Type of service we are getting</typeparam>
        /// <returns>Service asked for</returns>
        public T GetService<T>()
        {
            //Get Type object for the type of object we want to get
            Type t = typeof(T);

            //If the services dictionary has an object of that type stored, then return ie
            if (servicesDictionary.ContainsKey(t))
                return (T)servicesDictionary[t];

            //Else tell the user that something of that type is not stored
            else
                throw new ArgumentException("No Game Services exist for that type.");
        }

        /// <summary>
        /// Remove a stored service from the manager
        /// </summary>
        /// <typeparam name="T">Type of service we are removing</typeparam>
        public void RemoveService<T>()
        {
            //Remove it from the dictionary
            servicesDictionary.Remove(typeof(T));
        }

                                                #endregion METHODS
    }
}
