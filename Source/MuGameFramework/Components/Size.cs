using System;
using CSSize = System.Drawing.Size;
using SFMLVector2u = SFML.Window.Vector2u;

namespace MuGameFramework.Components
{
    /// <summary>
    /// Represents an objects Width and Height
    /// </summary>
    public struct Size
    {
                                                #region PROPERTIES
            
        /// <summary>
        /// Width of this Size object
        /// </summary>
        public int Width;

        /// <summary>
        /// Height of this Size object
        /// </summary>
        public int Height;

        /// <summary>
        /// Returns a size with the value of Zero
        /// </summary>
        public static readonly Size Zero = new Size(0, 0);

                                                #endregion PROPERTIES

                                                #region IMPLICIT PROPERTIES

        /// <summary>
        /// Get/Set this Size as a CSSize
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal CSSize AsCSSize
        {
            get { return new CSSize(Width, Height); }
            set { Width = value.Width; Height = value.Height; }
        }

        /// <summary>
        /// Get/Set this Size as a SFMLVector2u
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal SFMLVector2u AsSFMLVector2u
        {
            get { return new SFMLVector2u((uint)Width, (uint)Height); }
            set { Width = (int)value.X; Height = (int)value.Y; }
        }

                                                #endregion IMPLICIT PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of Size
        /// </summary>
        public Size(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Print out the values of a size as a string
        /// </summary>
        /// <param name="value">Size to print out</param>
        /// <returns>String of thus Size</returns>
        public static implicit operator string(Size value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Add two Sizes together
        /// </summary>
        /// <param name="a">Size A to add</param>
        /// <param name="b">Size B to add</param>
        /// <returns>Final size</returns>
        public static Size operator +(Size a, Size b)
        {
            return new Size(a.Width + b.Width, a.Height + b.Height);
        }

        /// <summary>
        /// Subtract two Sizes
        /// </summary>
        /// <param name="a">Size A to subtract</param>
        /// <param name="b">Size B to subtract</param>
        /// <returns>Final size</returns>
        public static Size operator -(Size a, Size b)
        {
            return new Size(a.Width - b.Width, a.Height - b.Height);
        }

        /// <summary>
        /// Multiply two sizes together
        /// </summary>
        /// <param name="a">Size A to multiply together</param>
        /// <param name="b">Size B to multiply together</param>
        /// <returns>Final size</returns>
        public static Size operator *(Size a, Size b)
        {
            return new Size(a.Width * b.Width, a.Height * b.Height);
        }

        /// <summary>
        /// Multiply a scalar to a size
        /// </summary>
        /// <param name="a">Size A to multiply</param>
        /// <param name="b">Scalar to multiply by</param>
        /// <returns>Final size</returns>
        public static Size operator *(Size a, int b)
        {
            return new Size(a.Width * b, a.Height * b);
        }

        /// <summary>
        /// Multiply a scalar to a size
        /// </summary>
        /// <param name="a">Scalar to multiply by</param>
        /// <param name="b">Size B to multiply</param>
        /// <returns>Final size</returns>
        public static Size operator *(int a, Size b)
        {
            return new Size(b.Width * a, b.Height * a);
        }

        /// <summary>
        /// Divide two sizes together
        /// </summary>
        /// <param name="a">Size A to divide together</param>
        /// <param name="b">Size B to divide together</param>
        /// <returns>Final size</returns>
        public static Size operator /(Size a, Size b)
        {
            return new Size(a.Width / b.Width, a.Height / b.Height);
        }

        /// <summary>
        /// Divide a scalar to a size
        /// </summary>
        /// <param name="a">Size A to divide</param>
        /// <param name="b">Scalar to divide by</param>
        /// <returns>Final size</returns>
        public static Size operator /(Size a, int b)
        {
            return new Size(a.Width / b, a.Height / b);
        }

        /// <summary>
        /// Divide a scalar to a size
        /// </summary>
        /// <param name="a">Scalar to divide by</param>
        /// <param name="b">Size B to divide</param>
        /// <returns>Final size</returns>
        public static Size operator /(int a, Size b)
        {
            return new Size(b.Width / a, b.Height / a);
        }

        /// <summary>
        /// Compare two sizes for equality
        /// </summary>
        /// <param name="a">Size A to check</param>
        /// <param name="b">Size B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(Size a, Size b)
        {
            //Check that either size is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.Width == b.Width && a.Height == b.Height);
        }

        /// <summary>
        /// Compare two sizes for inequality
        /// </summary>
        /// <param name="a">Size A to check</param>
        /// <param name="b">Size B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(Size a, Size b)
        {
            //Check that either size is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.Width != b.Width || a.Height != b.Height);
        }

                                                #endregion OPERATOR METHODS

                                                #region OVERRIDE METHODS

        /// <summary>
        /// Compare a size with this for equality
        /// </summary>
        /// <param name="obj">Size to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is Size)
                return (this == (Size)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this object
                hash = hash * 23 + Width.GetHashCode();
                hash = hash * 23 + Height.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Print out the values of a size as a string
        /// </summary>
        /// <returns>String of thus Size</returns>
        public override string ToString()
        {
            return "(" + this.Width + ", " + this.Height + ")";
        }

                                                #endregion OVERRIDE METHODS
    }
}
