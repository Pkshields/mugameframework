using System;
using CSPoint = System.Drawing.Point;
using SFMLVector2i = SFML.Window.Vector2i;

namespace MuGameFramework.Components
{
    /// <summary>
    /// Represents a 2D (X, Y) point on the screen
    /// </summary>
    public struct Point
    {
                                                #region PROPERTIES
            
        /// <summary>
        /// X coordinate of this Point object
        /// </summary>
        public int X;

        /// <summary>
        /// Y coordinate of this Point object
        /// </summary>
        public int Y;

        /// <summary>
        /// Returns a point with the value of Zero
        /// </summary>
        public static readonly Point Zero = new Point(0, 0);

                                                #endregion PROPERTIES

                                                #region IMPLICIT PROPERTIES

        /// <summary>
        /// Get/Set this Point as a CSPoint
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal CSPoint AsCSPoint
        {
            get { return new CSPoint(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

        /// <summary>
        /// Get/Set this Point as a SFMLVector2i
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal SFMLVector2i AsSFMLVector2i
        {
            get { return new SFMLVector2i(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

                                                #endregion IMPLICIT PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of Point
        /// </summary>
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Print out the values of a point as a string
        /// </summary>
        /// <param name="value">Point to print out</param>
        /// <returns>String of thus Point</returns>
        public static implicit operator string(Point value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Add two Points together
        /// </summary>
        /// <param name="a">Point A to add</param>
        /// <param name="b">Point B to add</param>
        /// <returns>Final point</returns>
        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        /// Subtract two Points
        /// </summary>
        /// <param name="a">Point A to subtract</param>
        /// <param name="b">Point B to subtract</param>
        /// <returns>Final point</returns>
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        /// Multiply two points together
        /// </summary>
        /// <param name="a">Point A to multiply together</param>
        /// <param name="b">Point B to multiply together</param>
        /// <returns>Final point</returns>
        public static Point operator *(Point a, Point b)
        {
            return new Point(a.X * b.X, a.Y * b.Y);
        }

        /// <summary>
        /// Multiply a scalar to a point
        /// </summary>
        /// <param name="a">Point A to multiply</param>
        /// <param name="b">Scalar to multiply by</param>
        /// <returns>Final point</returns>
        public static Point operator *(Point a, int b)
        {
            return new Point(a.X * b, a.Y * b);
        }

        /// <summary>
        /// Multiply a scalar to a point
        /// </summary>
        /// <param name="a">Scalar to multiply by</param>
        /// <param name="b">Point B to multiply</param>
        /// <returns>Final point</returns>
        public static Point operator *(int a, Point b)
        {
            return new Point(b.X * a, b.Y * a);
        }

        /// <summary>
        /// Divide two points together
        /// </summary>
        /// <param name="a">Point A to divide together</param>
        /// <param name="b">Point B to divide together</param>
        /// <returns>Final point</returns>
        public static Point operator /(Point a, Point b)
        {
            return new Point(a.X / b.X, a.Y / b.Y);
        }

        /// <summary>
        /// Divide a scalar to a point
        /// </summary>
        /// <param name="a">Point A to divide</param>
        /// <param name="b">Scalar to divide by</param>
        /// <returns>Final point</returns>
        public static Point operator /(Point a, int b)
        {
            return new Point(a.X / b, a.Y / b);
        }

        /// <summary>
        /// Divide a scalar to a point
        /// </summary>
        /// <param name="a">Scalar to divide by</param>
        /// <param name="b">Point B to divide</param>
        /// <returns>Final point</returns>
        public static Point operator /(int a, Point b)
        {
            return new Point(b.X / a, b.Y / a);
        }

        /// <summary>
        /// Compare two points for equality
        /// </summary>
        /// <param name="a">Point A to check</param>
        /// <param name="b">Point B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(Point a, Point b)
        {
            //Check that either point is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.X == b.X && a.Y == b.Y);
        }

        /// <summary>
        /// Compare two points for inequality
        /// </summary>
        /// <param name="a">Point A to check</param>
        /// <param name="b">Point B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(Point a, Point b)
        {
            //Check that either point is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.X != b.X || a.Y != b.Y);
        }

                                                #endregion OPERATOR METHODS

                                                #region OVERRIDE METHODS

        /// <summary>
        /// Compare a point with this for equality
        /// </summary>
        /// <param name="obj">Point to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is Point)
                return (this == (Point)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this object
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Print out the values of a point as a string
        /// </summary>
        /// <returns>String of thus Point</returns>
        public override string ToString()
        {
            return "(" + this.X + ", " + this.Y + ")";
        }

                                                #endregion OVERRIDE METHODS
    }
}
