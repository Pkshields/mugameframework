using System;
using CSRectangle = System.Drawing.Rectangle;
using SFMLIntRect = SFML.Graphics.IntRect;

namespace MuGameFramework.Components
{
    /// <summary>
    /// Represents a Rectangle on a 2D plane on the screen
    /// </summary>
    public struct Rectangle
    {
                                                #region PROPERTIES
            
        /// <summary>
        /// Position of this Rectangle object
        /// </summary>
        public Point Position;

        /// <summary>
        /// X coordinate of this Rectangle object
        /// </summary>
        public int X
        {
            get { return Position.X; }
            set { Position.X = value; }
        }

        /// <summary>
        /// Y coordinate of this Rectangle object
        /// </summary>
        public int Y
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }

        /// <summary>
        /// Width of this Vector2 object
        /// </summary>
        public int Width;

        /// <summary>
        /// Height of this Vector2 object
        /// </summary>
        public int Height;

        /// <summary>
        /// Top value of this Rectangle
        /// </summary>
        public int Top
        {
            get { return this.Y; }
            set { this.Y = value; }
        }

        /// <summary>
        /// Left value of this Rectangle
        /// </summary>
        public int Left
        {
            get { return this.X; }
            set { this.X = value; }
        }

        /// <summary>
        /// Bottom value of this Rectangle
        /// </summary>
        public int Bottom
        {
            get { return this.Y + this.Height; }
            set { this.Height = value - this.Y; }
        }

        /// <summary>
        /// Right value of this Rectangle
        /// </summary>
        public int Right
        {
            get { return this.X + this.Width; }
            set { this.Width = value - this.X; }
        }

        /// <summary>
        /// Center of the Rectangle
        /// </summary>
        public Vector2 Center
        {
            get { return new Vector2(this.X + (this.Width / 2f), this.Y + (this.Height / 2f)); }
        }

        /// <summary>
        /// Returns a Vector2 with the value of Zero
        /// </summary>
        public static readonly Rectangle Zero = new Rectangle(0, 0, 0, 0);

                                                #endregion PROPERTIES

                                                #region IMPLICIT PROPERTIES

        /// <summary>
        /// Get/Set this Rectangle as a CSRectangle
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal CSRectangle AsCSRectangle
        {
            get { return new CSRectangle(X, Y, Width, Height); }
            set { X = value.X; Y = value.Y; Width = value.Width; Height = value.Height; }
        }

        /// <summary>
        /// Get/Set this Rectangle as a SFMLIntRect
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal SFMLIntRect AsSFMLIntRect
        {
            get { return new SFMLIntRect(X, Y, Width, Height); }
            set { X = value.Left; Y = value.Top; Width = value.Width; Height = value.Height; }
        }

                                                #endregion IMPLICIT PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of Rectangle
        /// </summary>
        public Rectangle(int x, int y, int width, int height)
        {
            this.Position = new Point(x, y);
            this.Width = width;
            this.Height = height;
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Print out the values of a rectangle as a string
        /// </summary>
        /// <param name="value">Rectangle to print out</param>
        /// <returns>String of thus Rectangle</returns>
        public static implicit operator string(Rectangle value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Compare two Rectangles for equality
        /// </summary>
        /// <param name="a">Rectangle A to check</param>
        /// <param name="b">Rectangle B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(Rectangle a, Rectangle b)
        {
            //Check that either rectangle is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.X == b.X && a.Y == b.Y && a.Width == b.Width && a.Height == b.Height);
        }

        /// <summary>
        /// Compare two Rectangles for inequality
        /// </summary>
        /// <param name="a">Rectangle A to check</param>
        /// <param name="b">Rectangle B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(Rectangle a, Rectangle b)
        {
            //Check that either rectangle is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.X != b.X || a.Y != b.Y || a.Width != b.Width || a.Height != b.Height);
        }

                                                #endregion OPERATOR METHODS

                                                #region OVERRIDE METHODS

        /// <summary>
        /// Compare a rectangle with this for equality
        /// </summary>
        /// <param name="obj">Rectangle to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is Rectangle)
                return (this == (Rectangle)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this objetc
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                hash = hash * 23 + Width.GetHashCode();
                hash = hash * 23 + Height.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Print out the values of a rectangle as a string
        /// </summary>
        /// <returns>String of thus Rectangle</returns>
        public override string ToString()
        {
            return "(" + this.X + ", " + this.Y + ", " + this.Width + ", " + this.Height + ")";
        }

                                                #endregion OVERRIDE METHODS

                                                #region METHODS

        /// <summary>
        /// Check if a point is contained inside this rectangle
        /// </summary>
        /// <param name="point">Point to check</param>
        /// <returns>Result of check</returns>
        public bool Contains(Point point)
        {
            return (point.X >= this.Left &&
                    point.X <= this.Right &&
                    point.Y >= this.Top &&
                    point.Y <= this.Bottom);
        }

        /// <summary>
        /// Check if a point is contained inside this rectangle
        /// </summary>
        /// <param name="rect">Rectangle to check containment</param>
        /// <param name="point">Point to check</param>
        /// <returns>Result of check</returns>
        public static bool Contains(Rectangle rect, Point point)
        {
            return (point.X >= rect.Left &&
                    point.X <= rect.Right &&
                    point.Y >= rect.Top &&
                    point.Y <= rect.Bottom);
        }

        /// <summary>
        /// Check if a rectangle intersects this rectangle
        /// </summary>
        /// <param name="rect">Rectangle to check intersection</param>
        /// <returns>Result of check</returns>
        public bool Intersects(Rectangle rect)
        {
            //Check each of the four sides, seeing if any of them are outside of the scope of this rectangle
            //e.g., if rect.Top was 10 whereas this.Bottom was 5, they are obviously not intersecting]
            //If it returns true on any of them, then they are not intersecting
            //Else, they are!
            return !(rect.Top > this.Bottom ||
                    rect.Bottom < this.Top ||
                    rect.Left > this.Right ||
                    rect.Right < this.Left);
        }

        /// <summary>
        /// Check if two Rectangles are intersecting
        /// </summary>
        /// <param name="rect1">1st Rectangle to check intersection</param>
        /// <param name="rect2">2nd Rectangle to check intersection</param>
        /// <returns>Result of check</returns>
        public static bool Intersects(Rectangle rect1, Rectangle rect2)
        {
            //Reasoning in above method
            return !(rect2.Top > rect1.Bottom ||
                    rect2.Bottom < rect1.Top ||
                    rect2.Left > rect1.Right ||
                    rect2.Right < rect1.Left);
        }

        /// <summary>
        /// Inflate/Deflate the box's Width and Height
        /// </summary>
        /// <param name="changeWidth">Amount fo change needed to Width</param>
        /// <param name="changeHeight">Amount fo change needed to Height</param>
        public void Inflate(int changeWidth, int changeHeight)
        {
            this.Width += changeWidth;
            this.Height += changeHeight;
        }

                                                #endregion METHODS
    }
}
