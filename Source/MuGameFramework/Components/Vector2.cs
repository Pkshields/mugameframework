using System;
using CSPointF = System.Drawing.PointF;
using SFMLVector2f = SFML.Window.Vector2f;

namespace MuGameFramework.Components
{
    /// <summary>
    /// Represents a 2D Vector object on the screen
    /// </summary>
    public struct Vector2
    {
                                                #region PROPERTIES
            
        /// <summary>
        /// X coordinate of this Vector2 object
        /// </summary>
        public float X;

        /// <summary>
        /// Y coordinate of this Vector2 object
        /// </summary>
        public float Y;

        /// <summary>
        /// Returns a Vector2 with the value of Zero
        /// </summary>
        public static readonly Vector2 Zero = new Vector2(0, 0);

        /// <summary>
        /// Returns a Vector2 with the value of (1, 1)
        /// </summary>
        public static readonly Vector2 One = new Vector2(1, 1);

                                                #endregion PROPERTIES

                                                #region CONVERSIONS

        /// <summary>
        /// Get/Set this Vector2 as a CSPointF
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal CSPointF AsCSPointF
        {
            get { return new CSPointF(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

        /// <summary>
        /// Get/Set this Vector2 as a SFMLVector2f
        /// Used instead of implicit operators to avoid exceptions thrown on the users end
        /// </summary>
        internal SFMLVector2f AsSFMLVector2f
        {
            get { return new SFMLVector2f(X, Y); }
            set { X = value.X; Y = value.Y; }
        }

                                                #endregion CONVERSIONS

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create an instance of Vector2
        /// </summary>
        public Vector2(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

                                                #endregion CONSTRUCTORS

                                                #region OPERATOR METHODS

        /// <summary>
        /// Print out the values of a vector as a string
        /// </summary>
        /// <param name="value">Vector2 to print out</param>
        /// <returns>String of thus Vector2</returns>
        public static implicit operator string(Vector2 value)
        {
            return value.ToString();
        }

        /// <summary>
        /// Add two Vector2s together
        /// </summary>
        /// <param name="a">Vector2 A to add</param>
        /// <param name="b">Vector2 B to add</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        /// Subtract two Vector2s
        /// </summary>
        /// <param name="a">Vector2 A to subtract</param>
        /// <param name="b">Vector2 B to subtract</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        /// Multiply two Vector2s together
        /// </summary>
        /// <param name="a">Vector2 A to multiply together</param>
        /// <param name="b">Vector2 B to multiply together</param>
        /// <returns>Final point</returns>
        public static Vector2 operator *(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X * b.X, a.Y * b.Y);
        }

        /// <summary>
        /// Multiply a scalar to a Vector2
        /// </summary>
        /// <param name="a">Vector2 A to multiply</param>
        /// <param name="b">Scalar to multiply by</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator *(Vector2 a, float b)
        {
            return new Vector2(a.X * b, a.Y * b);
        }

        /// <summary>
        /// Multiply a scalar to a Vector2
        /// </summary>
        /// <param name="a">Scalar to multiply by</param>
        /// <param name="b">Vector2 B to multiply</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator *(float a, Vector2 b)
        {
            return new Vector2(b.X * a, b.Y * a);
        }

        /// <summary>
        /// Divide two Vector2s together
        /// </summary>
        /// <param name="a">Vector2 A to divide together</param>
        /// <param name="b">Vector2 B to divide together</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator /(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X / b.X, a.Y / b.Y);
        }

        /// <summary>
        /// Divide a scalar to a Vector2
        /// </summary>
        /// <param name="a">Vector2 A to divide</param>
        /// <param name="b">Scalar to divide by</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator /(Vector2 a, float b)
        {
            return new Vector2(a.X / b, a.Y / b);
        }

        /// <summary>
        /// Divide a scalar to a Vector2
        /// </summary>
        /// <param name="a">Scalar to divide by</param>
        /// <param name="b">Vector2 B to divide</param>
        /// <returns>Final Vector2</returns>
        public static Vector2 operator /(float a, Vector2 b)
        {
            return new Vector2(b.X / a, b.Y / a);
        }

        /// <summary>
        /// Compare two Vector2s for equality
        /// </summary>
        /// <param name="a">Vector2 A to check</param>
        /// <param name="b">Vector2 B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator ==(Vector2 a, Vector2 b)
        {
            //Check that either vector2 is not equal to null
            if ((object)a == null || (object)b == null)
                return false;

            //Return result
            return (a.X == b.X && a.Y == b.Y);
        }

        /// <summary>
        /// Compare two Vector2s for inequality
        /// </summary>
        /// <param name="a">Vector2 A to check</param>
        /// <param name="b">Vector2 B to check</param>
        /// <returns>Result of comparison</returns>
        public static bool operator !=(Vector2 a, Vector2 b)
        {
            //Check that either vector2 is not equal to null
            if ((object)a == null || (object)b == null)
                return true;

            //Return result
            return (a.X != b.X || a.Y != b.Y);
        }

                                                #endregion OPERATOR METHODS

                                                #region OVERRIDE METHODS

        /// <summary>
        /// Compare a Vector2 with this for equality
        /// </summary>
        /// <param name="obj">Vector2 to compare</param>
        /// <returns>Result of comparison</returns>
        public override bool Equals(object obj)
        {
            if (obj is Vector2)
                return (this == (Vector2)obj);

            return false;
        }

        /// <summary>
        /// Get the hash code for this object
        /// </summary>
        /// <returns>The hash code for this object</returns>
        public override int GetHashCode()
        {
            //HashCode algorithm taken from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
            //Int overflow in this case is fine, it will just wrap around and still come put with an equal hash to another same object
            unchecked
            {
                //Take a random prime number
                int hash = 17;

                //Generate the hash using another prime number and the hashes of each specific object in this objetc
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();

                return hash;
            }
        }

        /// <summary>
        /// Print out the values of a vector as a string
        /// </summary>
        /// <returns>String of thus Vector2</returns>
        public override string ToString()
        {
            return "(" + this.X + ", " + this.Y + ")";
        }

                                                #endregion OVERRIDE METHODS

                                                #region METHODS

        /// <summary>
        /// Calculate the length of a vector using Pythagoras' theorem
        /// </summary>
        /// <returns>Length of the vector</returns>
        public float Length()
        {
            return (float)Math.Sqrt((X * X) + (Y * Y));
        }

        /// <summary>
        /// Calculate the length of a vector using Pythagoras' theorem
        /// </summary>
        /// <param name="vector">Vector to get the length from</param>
        /// <returns>Length of the vector</returns>
        public static float Length(Vector2 vector)
        {
            return (float)Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
        }

        /// <summary>
        /// Normalize this Vector2
        /// </summary>
        public void Normalize()
        {
            float length = Length();
            X = X / length;
            Y = Y / length;
        }

        /// <summary>
        /// Get a unit vector of a Vector2
        /// </summary>
        /// <param name="vector">Vector to be normalized</param>
        /// <returns>Unit Vector2</returns>
        public static Vector2 Normalize(Vector2 vector)
        {
            float length = vector.Length();
            return new Vector2(vector.X / length, vector.Y / length);
        }
        
        /// <summary>
        /// Clamp this Vector2 between two values
        /// </summary>
        /// <param name="min">Minimum vector</param>
        /// <param name="max">Maximum vector</param>
        public void Clamp(Vector2 min, Vector2 max)
        {
            if (min.X > max.X || min.Y > max.Y)
                throw new ArgumentException("Minimum vector must be smaller than the maximum vector");

            if (X < min.X) X = min.X;
            else if (X > max.X) X = max.X;

            if (Y < min.Y) Y = min.Y;
            else if (Y > max.Y) Y = max.Y;
        }

        /// <summary>
        /// Get a vector of this Vector2, clamped between two values
        /// </summary>
        /// <param name="vector">Vector to be clamped</param>
        /// <param name="min">Minimum vector</param>
        /// <param name="max">Maximum vector</param>
        /// <returns>Clamped Vector2</returns>
        public static Vector2 Clamp(Vector2 vector, Vector2 min, Vector2 max)
        {
            if (min.X > max.X || min.Y > max.Y)
                throw new ArgumentException("Minimum vector must be smaller than the maximum vector");

            Vector2 newVector = new Vector2();

            if (vector.X < min.X) newVector.X = min.X;
            else if (vector.X > max.X) newVector.X = max.X;
            else newVector.X = vector.X;

            if (vector.Y < min.Y) newVector.Y = min.Y;
            else if (vector.Y > max.Y) newVector.Y = max.Y;
            else newVector.Y = vector.Y;

            return newVector;
        }

        /// <summary>
        /// Calculate the distance between two vectors
        /// </summary>
        /// <param name="vector">2nd vector</param>
        /// <returns>Distance between the vectors</returns>
        public float Distance(Vector2 vector)
        {
            float x1, y1;
            x1 = vector.X - X;
            y1 = vector.Y - Y;

            return (float)Math.Abs(Math.Sqrt((x1 * x1) + (y1 * y1)));
        }

        /// <summary>
        /// Calculate the distance between two vectors
        /// </summary>
        /// <param name="vector1">1st vector</param>
        /// <param name="vector2">2nd vector</param>
        /// <returns>Distance between the vectors</returns>
        public static float Distance(Vector2 vector1, Vector2 vector2)
        {
            float x1, y1;
            x1 = vector2.X - vector1.X;
            y1 = vector2.Y - vector1.Y;

            return (float)Math.Abs(Math.Sqrt((x1 * x1) + (y1 * y1)));
        }

        /// <summary>
        /// Get the dot product of this and another Vector2
        /// </summary>
        /// <param name="vector">2nd Vector</param>
        /// <returns>Dot product result</returns>
        public float DotProduct(Vector2 vector)
        {
            return ((this.X * vector.X) + (this.Y * vector.Y));
        }

        /// <summary>
        /// Get the dot product of two Vector2s
        /// </summary>
        /// <param name="vector1">1st Vector</param>
        /// <param name="vector2">2nd Vector</param>
        /// <returns>Dot product result</returns>
        public static float DotProduct(Vector2 vector1, Vector2 vector2)
        {
            return ((vector1.X * vector2.X) + (vector1.Y * vector2.Y));
        }

        /// <summary>
        /// Calculate the angle between two vectors
        /// </summary>
        /// <param name="vector">2nd Vector</param>
        /// <returns>Angle between the vectors (in Radians)</returns>
        public float AngleBetween(Vector2 vector)
        {
            try
            {
                return (float)Math.Acos((double)(this.DotProduct(vector) / (this.Length() * vector.Length())));
            }

            catch
            {
                throw new DivideByZeroException("Vectors are parallel, so no angle between them");
            }
        }

        /// <summary>
        /// Calculate the angle between two vectors
        /// </summary>
        /// <param name="vector1">1st Vector</param>
        /// <param name="vector2">2nd Vector</param>
        /// <returns>Angle between the vectors (in Radians)</returns>
        public static float AngleBetween(Vector2 vector1, Vector2 vector2)
        {
            try
            {
                return (float)Math.Acos((double)(vector1.DotProduct(vector2) / (vector1.Length() * vector2.Length())));
            }

            catch
            {
                throw new DivideByZeroException("Vectors are parallel, so no angle between them");
            }
        }

        /// <summary>
        /// Rotate this vector around a point
        /// </summary>
        /// <param name="point">Point to rotate around</param>
        /// <param name="angle">Angle to rotate</param>
        public void RotateAroundPoint(Vector2 point, float angle)
        {
            //Get the point relative to the "origin" we are rotating around
            Vector2 vector = this - point;

            //If the vector is already (0, 0), then it wont change, just return
            if (vector == Vector2.Zero)
                return;

            //Get the angle of this vector relative to the "origin" (using trig)
            float a = (float)Math.Atan2(vector.Y, vector.X);
            a += angle; //Add the angle of rotation

            //Use the math to change the direction of a vector to rotate it.
            //Using this method doesn't preserve magnitude, so multiply by the vector's length to apply magnitude again
            //Math taken from here: http://www.physicsforums.com/showthread.php?t=325959
            float length = vector.Length();
            vector = new Vector2((float)Math.Cos(a) * length, (float)Math.Sin(a) * length);

            //Get the new Vector2 relative to the real origin again
            vector = vector + point;

            //Apply transformation
            //Round to get rid of the margin of error provided by decimal PIs
            this.X = (float)Math.Round(vector.X, 6);
            this.Y = (float)Math.Round(vector.Y, 6);
        }

        /// <summary>
        /// Rotate a vector around a point
        /// </summary>
        /// <param name="vector">Vector to rotate</param>
        /// <param name="point">Point to rotate around</param>
        /// <param name="angle">Angle to rotate</param>
        public static Vector2 RotateAroundPoint(Vector2 vector, Vector2 point, float angle)
        {
            //Get the point relative to the "origin" we are rotating around
            Vector2 vector2 = vector - point;

            //If the vector is already (0, 0), then it wont change, just return
            if (vector2 == Vector2.Zero)
                return vector;

            //Get the angle of this vector relative to the "origin" (using trig)
            float a = (float)Math.Atan2(vector2.Y, vector2.X);
            a += angle; //Add the angle of rotation

            //Use the math to change the direction of a vector to rotate it.
            //Using this method doesn't preserve magnitude, so multiply by the vector's length to apply magnitude again
            //Math taken from here: http://www.physicsforums.com/showthread.php?t=325959
            float length = vector2.Length();
            vector2 = new Vector2((float)Math.Cos(a) * length, (float)Math.Sin(a) * length);

            //Get the new Vector2 relative to the real origin again
            vector2 = vector2 + point;

            //Apply transformation
            //Round to get rid of the margin of error provided by decimal PIs
            return new Vector2((float)Math.Round(vector2.X, 6), (float)Math.Round(vector2.Y, 6));
        }
                                                #endregion METHODS

    }
}
