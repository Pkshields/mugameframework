using System;
using System.Threading;
using System.Diagnostics;
using MuGameFramework.InputDevice;
using MuGameFramework.Graphics;

namespace MuGameFramework
{
    /// <summary>
    /// Core code behind the running of the Game Framework
    /// </summary>
    public class GameFramework : IDisposable
    {
                                                #region PROPERTIES

        /// <summary>
        /// Length of time allowed for each update
        /// </summary>
        private long updateLength;

        /// <summary>
        /// Decides if game runs at fixed to varaible timestep
        /// </summary>
        private bool isFixedTimestep;

        /// <summary>
        /// Window in which we will draw our game
        /// </summary>
        public Window Window
        {
            get;
            private set;
        }

        /// <summary>
        /// Timer used to keep the game moving at a fixed framerate
        /// </summary>
        private Stopwatch timer;

        /// <summary>
        /// Enables sprites to be drawn on the screen
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// Object used to keep track of the gane time while the engine is running
        /// </summary>
        public GameTime GameTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Content Manager used to load and unload content from the game
        /// </summary>
        public ContentManager Content
        {
            get;
            private set;
        }

        /// <summary>
        /// GameComponents List used to manage GameComponents
        /// </summary>
        public GameComponentList Components
        {
            get;
            private set;
        }

        /// <summary>
        /// Global access to a collection of services via GameServices
        /// </summary>
        public GameServices Services
        {
            get;
            private set;
        }

        /// <summary>
        /// Class object that contains the code for the game
        /// </summary>
        private GameCode gameCode;

                                                #endregion PROPERTIES

                                                #region CONSTRUCTOR

        /// <summary>
        /// Initialize an instance of the MuGameFramework
        /// </summary>
        /// <param name="gameCode">Source file for the games GameCode</param>
        public GameFramework(GameCode gameCode)
        {
            //Store the GameCode object for use
            this.gameCode = gameCode;

            //Get the game settings from the game code
            GameSettings settings = gameCode.GameSettings();
            
            //Calculate the length of time each update has depending on the framerate and if the game has a fixed timestep
            updateLength = (long)((1f / (float)settings.FixedFramerate) * 1000f);
            isFixedTimestep = settings.IsFixedTimestep;

            //Setup the game window
            //Run separate contructors if fullscreen or not
            if (settings.IsFullscreen)
                Window = new Window(settings.WindowName);
            else
                Window = new Window(settings.WindowName, settings.WindowSizeLoc);

            //Set the window for InputDevice to use
            Input.Initialize(Window);

            //Initialize everything else contained in the GameFramework
            spriteBatch = new SpriteBatch(Window);              //SpriteBatch
            Content = new ContentManager(settings.ContentDir);  //Content Manager
            timer = new Stopwatch();                            //Timer
            GameTime = new GameTime();                          //GameTime
            Components = new GameComponentList();               //GameComponentList
            Services = new GameServices();                      //GameServices
        }

                                                #endregion CONSTRUCTOR

                                                #region METHOD

        /// <summary>
        /// Start the game engine
        /// </summary>
        public void StartEngine()
        {
            //Execute the Initialize code from GameCode
            gameCode.PreInitialize(this);
            gameCode.LoadContent();
            gameCode.Initialize();

            //Start the game time
            GameTime.Start();

            //Start the timer then initialise the game loop
            timer.Reset();
            timer.Start();
            GameLoop();
        }

        /// <summary>
        /// Core of the game: Run the game loop
        /// </summary>
        private void GameLoop()
        {
            //While there is a window we can draw onto
            while (Window.IsOpen)
            {
                //Update the GameTime
                GameTime.Update();

                //Update the input devices
                Input.Update();

                //Update the events for the window
                Window.DispatchEvents();

                //Run the Update methods in the game code
                gameCode.Update();
                Components.Update();

                //Begin the SpriteBatch, run the draw code and then end the SpriteBatch
                spriteBatch.Begin();
                gameCode.Draw(spriteBatch);
                Components.Draw(spriteBatch);
                spriteBatch.End();

                //Elapse the remaining time we have left in the update, if any and if the game has a fixed timestep
                //Use Thread.Sleep instead of while loop to save CPU when not needed. Optimisation!
                if (timer.ElapsedMilliseconds < updateLength)
                {
                    GameTime.IsRunningSlow = false;

                    if (isFixedTimestep)
                        Thread.Sleep((int)(updateLength - timer.ElapsedMilliseconds));
                }
                else
                    GameTime.IsRunningSlow = true;

                //Restart the timer
                timer.Reset();
                timer.Start();
            }
        }

                                                #endregion METHODS

                                                #region PUBLIC METHODS

        /// <summary>
        /// Close the window and end the game system
        /// </summary>
        public void CloseGame()
        {
            Window.CloseWindow();
        }

                                                #endregion PUBLIC METHODS

                                                #region DISPOSE

        /// <summary>
        /// Dispose any and all objects, either managed or unmanaged at the end of this objects lifespan
        /// </summary>
        /// <param name="disposing">Dispose of Managed objects?</param>
        protected virtual void Dispose(bool disposing)
        {
            //Dispose of Managed components
            if (disposing)
            {
                if (Content != null)
                {
                    Content.Dispose();
                    Content = null;
                }

                if (spriteBatch != null)
                {
                    spriteBatch.Dispose();
                    spriteBatch = null;
                }
            }

            //Dispose of Unmanaged or large components
            if (Window != null)
            {
                Window.Dispose();
                Window = null;
            }
        }

        /// <summary>
        /// Used to explicitly free all resources currently in use by this object before destruction
        /// </summary>
        public void Dispose()
        {
            //Call the full Dispose code
            Dispose(true);

            //Then tell the Garbage Collector that it's work is already done
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose of this object
        /// </summary>
        ~GameFramework()
        {
            Dispose(false);
        }

                                                #endregion DISPOSE
    }
}