using System;

namespace MuGameFramework
{
    /// <summary>
    /// Object reresenting many values corresponding to the time in the game
    /// </summary>
    public class GameTime
    {
                                                #region PROPERTIES

        /// <summary>
        /// Total time game engine has been running
        /// </summary>
        public TimeSpan TotalTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Time since last game engine update
        /// </summary>
        public TimeSpan ElapsedTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Last time the game engine updated
        /// </summary>
        private TimeSpan lastTime = TimeSpan.Zero;

        /// <summary>
        /// Tells if the game is running slower than the framerate says
        /// </summary>
        public bool IsRunningSlow
        {
            get;
            internal set;
        }

                                                #endregion PROPERTIES

                                                #region CONSTRUCTORS

        /// <summary>
        /// Create a new instance of GameTime
        /// </summary>
        internal GameTime()
        {
            TotalTime = TimeSpan.Zero;
            ElapsedTime = TimeSpan.Zero;
            IsRunningSlow = false;
        }

                                                #endregion CONSTRUCTORS

                                                #region METHODS

        /// <summary>
        /// Start the GameTime ticking
        /// </summary>
        internal void Start()
        {
            //Initialize this with the current time of day
            lastTime = DateTime.UtcNow.TimeOfDay;
        }

        /// <summary>
        /// Update this GameTime object for this tick
        /// </summary>
        internal void Update()
        {
            //Ensure that the GameTime has been started
            if (lastTime != TimeSpan.Zero)
            {
                //Get a snapshot of the time of day
                TimeSpan timeOfDay = DateTime.UtcNow.TimeOfDay;

                //If the time of day is greater than the time it was last updated
                // then time has not ticked over to a new day
                if (timeOfDay > lastTime)
                {
                    //Get the new elapsed time
                    ElapsedTime = timeOfDay - lastTime;
                }

                //Else, time has ticked ver into 00:00
                else
                {
                    //Updated the new elapsed time, taking that into account
                    ElapsedTime = DateTime.MaxValue.TimeOfDay - lastTime;
                    ElapsedTime += timeOfDay;
                }

                //Update the total time and the last time updated, for the next update
                TotalTime += ElapsedTime;
                lastTime = timeOfDay;
            }
            else
                //Else throw an exception to show it hasn't started yet
                throw new InvalidOperationException("GameTime has not been started yet");
        }

                                                #endregion METHODS
    }
}
