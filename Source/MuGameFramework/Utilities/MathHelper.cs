using System;
using MuGameFramework.Components;

namespace MuGameFramework.Utilities
{
    /// <summary>
    /// Static class used to provide easy to access Math related methods from the framework
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Represents the Math.PI value for PiRepresents the Math.PI value for Pi
        /// </summary>
        public static readonly float Pi = (float)Math.PI;

        /// <summary>
        /// Represents the Math.PI value for Pi multiplied by two
        /// </summary>
        public static readonly float TwoPi = (float)Math.PI * 2f;

        /// <summary>
        /// Represents the Math.PI value for Pi divided by two
        /// </summary>
        public static readonly float PiOver2 = (float)Math.PI / 2f;

        /// <summary>
        /// Represents the Math.PI value for Pi divided by four
        /// </summary>
        public static readonly float PiOver4 = (float)Math.PI / 4f;

        /// <summary>
        /// Clip a number into the range of 0..1
        /// </summary>
        /// <param name="number">Number to be clipped</param>
        /// <returns>Clipped number</returns>
        public static float Saturate(float number)
        {
            if (number > 1)
                return 1;
            if (number < 0)
                return 0;
            else
                return number;
        }

        /// <summary>
        /// Clamp a number between two values
        /// </summary>
        /// <param name="num">Number to clamp</param>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns>Clamped number</returns>
        public static float Clamp(float num, float min, float max)
        {
            if (min > max)
                throw new ArgumentException("Minimum value must be smaller than the maximum value");

            if (num < min) return min;
            else if (num > max) return max;

            return num;
        }

        /// <summary>
        /// Convert an angle in Radians to Degrees
        /// </summary>
        /// <param name="angle">Angle in Radians</param>
        /// <returns>Angle in Degrees</returns>
        public static float RadiansToDegrees(float angle)
        {
            return (float)(angle * (180 / Math.PI));
        }

        /// <summary>
        /// Convert an angle in Degrees to Radians
        /// </summary>
        /// <param name="angle">Angle in Degrees</param>
        /// <returns>Angle in Radians</returns>
        public static float DegreesToRadians(float angle)
        {
            return (float)(angle * (Math.PI / 180));
        }

        /// <summary>
        /// Rotate a point around a given origin
        /// </summary>
        /// <param name="point">Point to rotate</param>
        /// <param name="origin">Origin to rotate around</param>
        /// <param name="rotation">Angle to rotate (in radians)</param>
        /// <returns>Rotated point</returns>
        public static Vector2 RotatePoint(Vector2 point, Vector2 origin, float rotation)
        {
            Vector2 translation = point - origin, 
                    returnPoint = new Vector2();
            float cosValue = (float)Math.Cos(rotation),
                  sinValue = (float)Math.Sin(rotation);

            returnPoint.X = (translation.X * cosValue) - (translation.Y * sinValue);
            returnPoint.Y = (translation.Y * cosValue) + (translation.X * sinValue);

            return returnPoint + origin;
        }

        /// <summary>
        /// Rotate a point around a given origin
        /// </summary>
        /// <param name="point">Point to rotate</param>
        /// <param name="origin">Origin to rotate around</param>
        /// <param name="rotation">Angle to rotate (in radians)</param>
        /// <returns>Rotated point</returns>
        public static Point RotatePoint(Point point, Point origin, float rotation)
        {
            Point translation = point - origin;
            translation.X = (int)((double)translation.X * Math.Cos(rotation) - (double)translation.Y * Math.Sin(rotation));
            translation.Y = (int)((double)translation.Y * Math.Cos(rotation) + (double)translation.X * Math.Sin(rotation));
            return translation + origin;
        }

        /// <summary>
        /// Find the scalar distance between two points
        /// </summary>
        /// <param name="point1">Point on screen</param>
        /// <param name="point2">Another point on screen</param>
        /// <returns>Scalar distance</returns>
        public static float DistanceBetween(Vector2 point1, Vector2 point2)
        {
            //Find the vertical and horizontal lines on right angle triangle
            double dx = point2.X - point1.X,
                   dy = point2.Y - point1.Y;

            //Pythag to find distance
            return (float)Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
        }

        /// <summary>
        /// Find the scalar distance between two points
        /// </summary>
        /// <param name="point1">Point on screen</param>
        /// <param name="point2">Another point on screen</param>
        /// <returns>Scalar distance</returns>
        public static float DistanceBetween(Point point1, Point point2)
        {
            //Find the vertical and horizontal lines on right angle triangle
            double dx = point2.X - point1.X,
                   dy = point2.Y - point1.Y;

            //Pythag to find distance
            return (float)Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));
        }
    }
}
