﻿using System;

namespace MuGameFramework
{
    /// <summary>
    /// Exception representing when something is out of range of the Joystick
    /// </summary>
    public class JoystickOutOfRangeException : Exception
    {
        /// <summary>
        /// Throw an exception when something on the Joystick is out of range
        /// </summary>
        public JoystickOutOfRangeException()
        { }

        /// <summary>
        /// Throw an exception when something on the Joystick is out of range
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        public JoystickOutOfRangeException(string message)
            : base(message)
        { }

        /// <summary>
        /// Throw an exception when something on the Joystick is out of range
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        /// <param name="inner">The exception that is the cause for the current exception.</param>
        public JoystickOutOfRangeException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}