﻿using System;

namespace MuGameFramework
{
    /// <summary>
    /// Exception representing when something is unsopported by Mono
    /// </summary>
    public class MonoUnsupportedException : Exception
    {
        /// <summary>
        /// Throw an exception for a piece of code that Mono does not support
        /// </summary>
        public MonoUnsupportedException()
        { }

        /// <summary>
        /// Throw an exception for a piece of code that Mono does not support
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        public MonoUnsupportedException(string message)
            : base(message)
        { }

        /// <summary>
        /// Throw an exception for a piece of code that Mono does not support
        /// </summary>
        /// <param name="message">The message that describes the error</param>
        /// <param name="inner">The exception that is the cause for the current exception.</param>
        public MonoUnsupportedException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}