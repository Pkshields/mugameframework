﻿using System;

namespace MuGameFramework.Utilities
{
    /// <summary>
    /// Collection of classes used by MuGameFramework internally
    /// </summary>
    internal static class Internal
    {
        /// <summary>
        /// Check if the framework is running on Mono
        /// </summary>
        /// <returns>If we are running on Mono</returns>
        public static bool IsMono()
        {
            //Checks if a Mono specific namespace exists
            return (Type.GetType("Mono.Runtime") != null);
        }
    }
}
